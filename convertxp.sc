__config()-> {
    'stay_loaded' -> true,
    'scope' -> 'player',
    'commands' -> {
        '<amount>' -> '__convert_xp_to_bottles',
    },
    'arguments' -> {
        'amount' -> {
            'type' -> 'int',
            'min' -> 1,
            'suggester' -> _(args)-> (
                p = player();
                suggestions = [];
                current_xp = p~'xp';
                if(current_xp >= 7, put(suggestions, null, 1));
                if(current_xp >= 70, put(suggestions, null, 10));
                put(suggestions, null, floor(current_xp / 7));
                suggestions;
            ),
        },
    },
};


__convert_xp_to_bottles(amount)-> (
    p = player();
    current_xp = p~'xp';
    xp_required = amount * 7;
    if(current_xp >= xp_required, (
        run('give ' + p~'name' + ' minecraft:experience_bottle ' + str(amount));
        modify(p, 'add_xp', -1 * xp_required);
    ), (
        display_title(p, 'actionbar', format('r You do not have enough XP'));
    ));
);