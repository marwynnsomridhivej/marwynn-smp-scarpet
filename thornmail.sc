import('lib', '__apply_damage');


__config()-> {
    'stay_loaded' -> 'true',
    'scope' -> 'player',
    'command_permission' -> 'ops',
    'commands' -> {
        '<player>' -> '__give_thornmail',
    },
    'arguments' -> {
        'player' -> {
            'type' -> 'players',
        },
    },
};


__give_thornmail(t)-> (
    target = player(get(t, 0));
    run('give ' + target + ' minecraft:leather_chestplate{display:{Name:\'[{"text":"Thornmail","italic":true,"color":"aqua"}]\',Lore:[\'[{"text":"Unique - Thorns: When struck by","italic":false}]\',\'[{"text":"an attack on-hit, deal 10 (+25%","italic":false}]\',\'[{"text":"bonus armor) damage to the","italic":false}]\',\'[{"text":"attacker and, if they are a","italic":false}]\',\'[{"text":"player, inflict them with Grievous","italic":false}]\',\'[{"text":"wounds for 3 seconds","italic":false}]\']},Enchantments:[{id:blast_protection,lvl:255},{id:vanishing_curse,lvl:1},{id:fire_protection,lvl:255},{id:mending,lvl:1},{id:projectile_protection,lvl:255},{id:protection,lvl:255},{id:unbreaking,lvl:255}],HideFlags:1,Unbreakable:1,Thornmail:true,AttributeModifiers:[{AttributeName:"generic.armor_toughness",Amount:100,Slot:chest,Name:"generic.armor_toughness",UUID:[I;-12366,31394,20025,-62788]},{AttributeName:"generic.armor",Amount:100,Slot:chest,Name:"generic.armor",UUID:[I;-12366,31494,20025,-62988]},{AttributeName:"generic.knockback_resistance",Amount:0.1,Slot:chest,Name:"generic.knockback_resistance",UUID:[I;-12366,31594,20025,-63188]}]}');
    print(player(), 'Gave ' + format('c ' + target) + ' a thornmail');
    print(target, 'You have been given a special leather chestplate');
    run('playsound minecraft:ui.toast.challenge_complete master ' + target~'name');
);