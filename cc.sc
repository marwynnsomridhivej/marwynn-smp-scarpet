__config()-> (
    m(
        l('stay_loaded', 'true'),
        l('scope', 'player'),
    )
);

__command()-> (
    p = player();
    pos_all = [floor(get(pos(p), 0)), floor(get(pos(p), 2))];
    if(
        p ~ 'dimension' == 'overworld', print('Your Nether Coords are: ' + format(str('b %d %d', map(pos_all, _ / 8)))),
        p ~ 'dimension' == 'the_nether', print('Your Overworld Coords are: ' + format(str('b %d %d', map(pos_all, _ * 8)))),
        p ~ 'dimension' == 'the_end', print('You must be in either the Overworld or the Nether to use this'),
    );
    return(null);
);