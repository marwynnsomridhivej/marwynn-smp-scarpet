__config()-> (
    m(
        l('stay_loaded', 'true'),
        l('scope', 'player'),
    )
);

__command()-> (
    run(str('kick %s go next', player()~'command_name'));
    return(null);
);