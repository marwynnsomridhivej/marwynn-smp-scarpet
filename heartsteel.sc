__config()-> {
    'stay_loaded' -> 'true',
    'scope' -> 'player',
};


_d = load_app_data();
if(!_d, (
    _d = nbt('{}');
    store_app_data(_d);
));


global_valid_mobs = [
    'blaze',
    'bogged',
    'breeze',
    'cave_spider',
    'creeper',
    'drowned',
    'elder_guardian',
    'ender_dragon',
    'enderman',
    'endermite',
    'evoker',
    'ghast',
    'guardian',
    'hoglin',
    'husk',
    'magma_cube',
    'phantom',
    'piglin',
    'piglin_brute',
    'pillager',
    'ravager',
    'shulker',
    'silverfish',
    'skeleton',
    'spider',
    'slime',
    'stray',
    'vex',
    'vindicator',
    'warden',
    'witch',
    'wither',
    'wither_skeleton',
    'zoglin',
    'zombie',
    'zombified_piglin',
    'zombie_villager',
    'player',
];


__get_player_heartsteel_level(p)-> {
    chestplate = query(p, 'holds', 'chest');
    if(chestplate != null, (
        [item_name, item_amount, item_nbt] = chestplate;
        return(item_nbt:'components.minecraft:enchantments.levels.marwynn:heartsteel');
    ), return(0));
};


__get_current_heartsteel_stacks(p)-> {
    [chestplate_name, chestplate_amount, chestplate_nbt] = query(p, 'holds', 'chest');
    modifiers = chestplate_nbt:'components.minecraft:attribute_modifiers.modifiers';
    if(modifiers, (
        for(parse_nbt(modifiers), (
            if(_:'id' == 'marwynn:heartsteel', (return(_:'amount')));
        ));
    ));
    return(0);
};


__on_player_attacks_entity(p, e)-> {
    heartsteel_lvl = __get_player_heartsteel_level(p);
    if(heartsteel_lvl > 0 && global_valid_mobs~(e~'type') != null && rand(1) <= heartsteel_lvl^2 / 400, (
        current_stacks = __get_current_heartsteel_stacks(p);
        if(current_stacks < 100, (
            run('item modify entity @s armor.chest [{"function": "minecraft:set_attributes","modifiers": [{"id": "marwynn:heartsteel","attribute": "minecraft:max_health","amount": ' + (current_stacks + 1) + 'd,"operation": "add_value","slot": "chest"}],"replace": false}]');
        ));
        run('playsound marwynn:heartsteel.proc master @a ' + join(' ', p~'pos')+ ' 0.7');
        run('advancement grant @s only marwynn:heartsteel_proc');
    ));
    return(null);
};


// =========================================
// ============ HP MANIPULATION ============
// =========================================


__init_data(p)-> {
    data = load_app_data();
    if(!data:(p~'uuid'), (
        max_health = query(p, 'attribute', 'max_health');
        data:(p~'uuid') = {
            'ratio' -> p~'health' / max_health,
            'had_heartsteel_equipped' -> false,
            'max_hp' -> query(p, 'attribute', 'max_health'),
            'previous_hp' -> p~'health',
        };
        store_app_data(data);
    ));
    return(data);
};


__save_player_hp_percentage(p)-> {
    data = __init_data(p);
    current_hp = p~'health';
    current_max_hp = query(p, 'attribute', 'max_health');
    previous_hp = data:(p~'uuid'):'previous_hp',
    previous_max_hp = data:(p~'uuid'):'max_hp';
    data:(p~'uuid') = {
        'ratio' -> previous_hp / previous_max_hp,
        'max_hp' -> current_max_hp,
        'had_heartsteel_equipped' -> bool(__get_player_heartsteel_level(p)),
        'previous_hp' -> current_hp,
    };
    store_app_data(data);
};


__restore_player_hp_percentage(p, retry, root)-> {
    data = __init_data(p);
    if(data:(p~'uuid'):'had_heartsteel_equipped', (
        current_saturation = p~'saturation';
        ratio = data:(p~'uuid'):'ratio';
        max_hp = data:(p~'uuid'):'max_hp';
        new_hp = max_hp * ratio;
        modify(p, 'health', new_hp);
        modify(p, 'saturation', current_saturation);
        if(retry && (round(p~'health') != round(new_hp)), (
            schedule(2, '__restore_player_hp_percentage', p, true, false);
        ));
    ));
    if(root, (
        handle_event('tick', '__per_tick_updater');
    ));
};


__init_player_restore_and_listener()-> {
    p = player();
    schedule(2, '__restore_player_hp_percentage', p, true, true);
};


__on_start()-> {
    __init_player_restore_and_listener();
};


__on_player_connects(p)-> {
    __init_player_restore_and_listener();
};


__on_player_disconnects(p, reason)-> {
    p = player();
    __save_player_hp_percentage(p);
    handle_event('tick', null);
};


__per_tick_updater()-> {
    p = player();
    data = __init_data(p);
    current_max_health = query(p, 'attribute', 'max_health');
    previous_max_health = data:(p~'uuid'):'max_hp';
    previous_health = data:(p~'uuid'):'previous_hp';

    if(previous_max_health != current_max_health, (
        ratio = previous_health / previous_max_health;
        modify(p, 'health', current_max_health * ratio);
    ));
    __save_player_hp_percentage(p);
};
