__config()-> (
    m(
        l('stay_loaded', 'true'),
        l('scope', 'player'),
    )
);


global_allow_show = true;


__on_player_dies(player)-> (
    data = load_app_data();
    if (!data, data = nbt('{}'));
    tag = nbt('{}');
    for(pos(player), put(tag:'DeathPos',str('%.6fd',_),_i));
    put(tag:'Dimension', player~'dimension');
    data:(player~'name') = tag;
    store_app_data(data);
    allow_show = true;
);


__on_player_respawns(player)-> (
    if(global_allow_show, (
        data = load_app_data();
        player_tag = data:(player~'name');
        death_pos = player_tag:'DeathPos.[]';
        dimension = title(replace(player_tag:'Dimension', 'the_', ''));
        death_msg = 'You died at ' + format('wb ' + str('%d %d %d', death_pos) + ' ', 'w in the ', 'wb ' + dimension);
        print(player, death_msg);
    ));
    global_allow_show = true;
);


__on_player_changes_dimension(player, from_pos, from_dimension, to_pos, to_dimension)-> (
    if(from_dimension == 'the_end' && to_dimension == 'overworld', global_allow_show = false);
);


__command()-> (
    name = player()~'command_name';
    data = load_app_data();
    if (
        !data, (
            print(player(), 'You haven\'t died yet, congrats!');
        ),
        !data:name, (
            print(player(), 'You haven\'t died yet, congrats!');
        ),
        print(player(), 'You died at ' + format('wb ' + str('%d %d %d', data:name:'DeathPos.[]') + ' ', 'w in the ', 'wb ' + title(replace(data:name:'Dimension', 'the_', ''))));
    );
    return(null);
)