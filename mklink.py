import os
import sys


scripts = [
    "actionbar_warning",
    "backpack",
    "banana",
    "convertxp",
    "deathboard",
    "drill_nbt",
    "drill",
    "heartsteel",
    "home",
    "lib",
    "offsets",
    "placement_nbt",
    "placement",
    "s",
    "smite",
    "suppressor",
    "toolping",
]


if __name__ == "__main__":
    target = sys.argv[1]
    _type = "datapack" if "datapack" in target else "resourcepack" if "resourcepack" in target else "script"
    if _type == "script":
        for script in scripts:
            extension = "sc" if os.path.exists(f"./{script}.sc") else "scl"
            path = os.path.join(os.getcwd(), f"./{script}.{extension}")
            try:
                os.symlink(path, f"{target}/{script}.{extension}")
                print(f"Symlink made for {script}.{extension}")
            except FileExistsError:
                print(f"Skipping {script}.{extension}, symlink already exists")
    elif _type == "datapack":
        for datapack in [dp for dp in os.listdir("./dp-rp") if os.path.exists(f"./dp-rp/{dp}/{dp} - DP.zip")]:
            path = os.path.join(os.getcwd(), f"./dp-rp/{datapack}/{datapack} - DP.zip")
            try:
                os.symlink(path, f"{target}/{datapack} - DP.zip")
                print(f"Symlink made for {datapack}")
            except FileExistsError:
                print(f"Skipping {datapack}, symlink already exists")
    elif _type == "resourcepack":
        for resourcepack in [rp for rp in os.listdir("./dp-rp") if os.path.exists(f"./dp-rp/{rp}/{rp} - RP.zip")]:
            path = os.path.join(os.getcwd(), f"./dp-rp/{resourcepack}/{resourcepack} - RP.zip")
            try:
                os.symlink(path, f"{target}/{resourcepack} - RP.zip")
                print(f"Symlink made for {resourcepack}")
            except FileExistsError:
                print(f"Skipping {resourcepack}, symlink already exists")