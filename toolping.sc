import('lib', '__set_notify');
__config()-> {
    'scope' -> 'player',
    'stay_loaded' -> true,
    'commands' -> {
        '' -> '__status',
        'on' -> '__enable_ping',
        'enable' -> '__enable_ping',
        'off' -> '__disable_ping',
        'disable' -> '__disable_ping',
    },
};


global_max_damage = {
    'wooden' -> 59,
    'stone' -> 131,
    'iron' -> 250,
    'golden' -> 32,
    'diamond' -> 1561,
    'netherite' -> 2031,
    'flint_and_steel' -> 64,
    'brush' -> 64,
    'fishing_rod' -> 64,
    'carrot_on_a_stick' -> 25,
    'warped_fungus_on_a_stick' -> 100,
    'shears' -> 238,
    'shield' -> 336,
    'bow' -> 384,
    'trident' -> 250,
    'crossbow' -> 465,
    'mace' -> 500,
};


_d = load_app_data();
if(!_d, (
    _d = nbt('{}');
    store_app_data(_d);
));


global_enabled = false;
global_warned = false;


__ensure_safe_nbt(p) -> {
    data = load_app_data();
    if(data:(p~'uuid') == null, (
        put(data, p~'uuid', false);
    ));
    return(data);
};


__on_start()-> {
    p = player();
    data = __ensure_safe_nbt(p);
    global_enabled = data:(p~'uuid');
    global_warned = false;
    __set_notify(p, 'tool ping', global_enabled);
};


__set(p, status) -> {
    if(status != global_enabled, (
        data = __ensure_safe_nbt(p);
        put(data, p~'uuid', status);
        global_enabled = status;
        __set_notify(p, 'tool ping', global_enabled);
        store_app_data(data);
        return(true);
    ));
    return(false);
};


__get_max_damage(name) -> {
    if(global_max_damage:name != null, return(global_max_damage:name));
    tool_tier = split('_', name):0;
    return(global_max_damage:tool_tier);
};


__on_player_swings_hand(p, hand) -> {
    if(!global_enabled || global_warned, exit());

    item = query(p, 'holds', hand);
    if(item == null, exit());

    [n, a, _nbt] = item;
    damage = _nbt:'components':'"minecraft:damage"';
    if(damage == null, exit());

    max_damage = __get_max_damage(n);
    threshold = min(100, max_damage * 0.1);
    if(max_damage - damage <= threshold, (
        run('playsound marwynn:toolping.proc master ' + p~'name' + ' ' + join(' ', p~'pos') + ' 1');
        global_warned = true;
    ));
};


__on_player_swaps_hands(p) -> global_warned = false;


__on_player_switches_slot(p, f, t) -> global_warned = false;


__status() -> {
    p = player();
    print(p, 'Tool ping on low durability is ' + if(global_enabled, format('c ENABLED'), format('c DISABLED')));
};


__enable_ping()-> {
    p = player();
    if(__set(p, true),
        print(p, 'Tool ping on low durability is ' + format('e ENABLED')),
        print(p, format('r Tool ping on low durability is already enabled')),
    );
};


__disable_ping()-> {
    p = player();
    if(__set(p, false),
        print(p, 'Tool ping on low durability is ' + format('e DISABLED')),
        print(p, format('r Tool ping on low durability is already disabled')),
    );
};