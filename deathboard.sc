__config()-> {
    'stay_loaded' -> true,
    'scope' -> 'player',
    'commands' -> {
        'enable' -> '__toggle_on',
        'on' -> '__toggle_on',
        'disable' -> '__toggle_off',
        'off' -> '__toggle_off',
    }
};


_d = load_app_data();
if(!_d, (
    _d = nbt('{}');
    store_app_data(_d);
));


global_team_name = 'marwynn.deathboard';


__init_data(p)-> {
    data = load_app_data();
    if(!data:(p~'uuid'), (
        data:(p~'uuid') = false;
        store_app_data(data);
    ));
    return(data);
};


__check_visibility(p)-> {
    data = __init_data(p);
    return(data:(p~'uuid'));
};


__save_visibility_status(p, status)-> {
    data = __init_data(p);
    data:(p~'uuid') = status;
    store_app_data(data);
};


__on_server_starts()-> {
    team_add(global_team_name);
    team_property(global_team_name, 'color', 'white');
    run('scoreboard objectives add Deaths deathCount "Deaths LMAO"');
    scoreboard_display('sidebar.team.white', 'Deaths');
};


__deathboard_visibility(p, status, _save)-> {
    if(status, team_add(global_team_name, p~'name'), team_leave(p~'name'));
    if(_save, __save_visibility_status(p, status));
};


__toggle_on()-> {
    p = player();
    if(!__check_visibility(p), (
        print(p, 'Deathboard is now ' + format('e visible'));
        __deathboard_visibility(p, true, true);
    ), print(p, format('r Deathboard is already visible')));
};


__toggle_off()-> {
    p = player();
    if(__check_visibility(p), (
        print(p, 'Deathboard is now ' + format('e hidden'));
        __deathboard_visibility(p, false, true);
    ), print(p, format('r Deathboard is already hidden')));
};