import('lib', 'global_allowed_chestplates', '__apply_only_lore');


__config()-> {
    'stay_loaded' -> 'true',
    'scope' -> 'player',
    'commands' -> {
        '' -> '__give_deaths_dance',
    },
};


__find_unenchanted_shield(p)-> (
    while((slot = inventory_find(p, 'shield', slot)) != null && slot < 41, 41, (
        [_sname, _samount, shield_nbt] = inventory_get(p, slot);
        if(!shield_nbt~'Enchantments', return(slot), slot += 1);
    ));
    return(null);
);


__check_craftable(p)-> (
    chest = query(p, 'holds', 'chest');
    if(chest != null, (
        [chestplate_name, _camount, chestplate_nbt] = chest;
        if(global_allowed_chestplates~chestplate_name != null, (
            if(!chestplate_nbt~'DeathsDance', (
                shield_slot = __find_unenchanted_shield(p);
                if(shield_slot != null, (
                    magma_cream_slot = inventory_find(p, 'magma_cream');
                    if(magma_cream_slot != null, (
                        [_mname, magma_cream_amount, _mnbt] = inventory_get(p, magma_cream_slot);
                        if(magma_cream_amount >= 4, (
                            return([shield_slot, magma_cream_slot, magma_cream_amount]);
                        ), print(p, format('r You need at least 4 magma cream to craft bloodthirster')));
                    ), print(p, format('r You need at least 4 magma cream to craft bloodthirster')));
                ), print(p, format('r You need at least 1 unenchanted shield to craft Deaths Dance')));
            ), print(p, format('r Deaths Dance has already been applied to your chestplate')));
        ), print(p, format('r Deaths Dance cannot be applied to your chestplate')));
    ), print(p, format('r You must be wearing a chestplate')));
    return(false);
);


__craft_deaths_dance(p, shield_slot, magma_cream_slot, magma_cream_amount)-> (
    [chestplate_name, chestplate_amount, chestplate_nbt] = inventory_get(p, 38);
    chestplate_nbt:'DeathsDance' = true;
    new_nbt = __apply_only_lore(chestplate_nbt, [
        '[{"text":"Death\'s Dance Applied:","italic":false,"color":"#ff6633","underlined":true}]',
        '[{"text":"For every instance of damage","italic":true,"color":"#ff6633"}]',
        '[{"text":"you would normally receive,","italic":true,"color":"#ff6633"}]',
        '[{"text":"instead take ","italic":true,"color":"#ff6633"},{"text":"50%","color":"#fcad03"},{"text":" of that","color":"#ff6633","italic":true}]',
        '[{"text":"damage immediately, and the","italic":true,"color":"#ff6633"}]',
        '[{"text":"other ","italic":true,"color":"#ff6633"},{"text":"50%","color":"#fcad03"},{"text":" is applied as a","color":"#ff6633","italic":true}]',
        '[{"text":"burn over ","italic":true,"color":"#ff6633"},{"text":"5","color":"#fcad03"},{"text":" seconds"}]'
    ]);
    inventory_set(p, 38, chestplate_amount, chestplate_name, new_nbt);
    inventory_set(p, shield_slot, 0);
    inventory_set(p, magma_cream_slot, magma_cream_amount - 4);
    print(p, 'Deaths Dance has been applied to your chestplate');
);


__give_deaths_dance()-> (
    p = player();
    craftable = __check_craftable(p);
    if(craftable, (
        __craft_deaths_dance(p, ...craftable);
        run('playsound minecraft:ui.toast.challenge_complete master ' + p~'name');
    ));
);