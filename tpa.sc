import('lib', '__assert_player_can_tp_return');

__config()-> {
    'stay_loaded' -> 'true',
    'scope' -> 'player',
    'commands' -> {
        '<player>' -> '__warp',
        'accept <playerAcceptable>' -> '__accept_warp',
        'reject <playerRejectable>' -> '__reject_warp',
        'cancel <playerCancelable>' -> '__cancel',
        'requests' -> '__see_pending',
        'ls' -> '__see_pending',
        '' -> '__see_pending',
    },
    'arguments' -> {
        'player' -> {
            'type' -> 'term',
            'suggester' -> _(args) -> (
                nameset = {};
                for(player('all'), if(_ != player(), nameset += _));
                keys(nameset);
            )
        },
        'playerAcceptable' -> {
            'type' -> 'term',
            'suggester' -> _(args) -> (
                nameset = {};
                p = player();
                requested = __get_all_requested(p);
                for(requested, nameset += _);
                if(nameset, nameset += '.');
                keys(nameset);
            )
        }, 
        'playerRejectable' -> {
            'type' -> 'term',
            'suggester' -> _(args) -> (
                nameset = {};
                p = player();
                requested = __get_all_requested(p);
                for(requested, nameset += _);
                if(nameset, nameset += '.');
                keys(nameset);
            )
        }, 
        'playerCancelable' -> {
            'type' -> 'term',
            'suggester' -> _(args) -> (
                nameset = {};
                p = player();
                requests = __view_all_requests(p);
                for(requests, nameset += _);
                if(nameset, nameset += '.');
                keys(nameset);
            )
        }, 
    },
};


_d = load_app_data();
if(!_d, (
    _d = nbt('{Requests:{}}');
    store_app_data(_d)
));


__on_player_connects(p)-> (
    requested = __get_all_requested(p);
    if(requested != null, (
        print(p, 'You have the following teleport requests for consideration:');
        for(requested, print(p, ' - ' + safe_player(_) + ' [' + format('be ACCEPT', '^i Accept Request', '!/tpa accept ' + _) + ' | ' + format('br REJECT', '^i Reject Request', '!/tpa reject ' + _) + ']'));
    ));
);


safe_print(name, text)-> (
    p = player(name);
    if(p != null, print(p, text));
);


safe_player(name)-> (
    p = player(name);
    if(p != null, return(p~'name'), return(name));
);


__init_nbt(p)-> (
    data = load_app_data();
    if(!data:('Requests.' + lower(p)), (
        data:('Requests.' + lower(p)) = nbt('[]');
    ));
    return(data);
);


__check_requests(p, target)-> (
    data = __init_nbt(p);
    requests = parse_nbt(data:('Requests.' + lower(p)));
    return(requests~lower(target) != null);
);


__get_requesteds(p, requester, requesteds)-> (
    if(requester != lower(p~'name'), (if(requesteds~(lower(p~'name')) != null, return(requester), return(null))));
    return(null);
);


__get_all_requested(p)-> (
    data = parse_nbt(__init_nbt(p):'Requests');
    requesteds = [];
    for(pairs(data), (
        r = __get_requesteds(p, ..._);
        if(r, requesteds += r);
    ));
    if(requesteds, return(requesteds), return(null));
);


__view_all_requests(p)-> (
    data = __init_nbt(p);
    requests = parse_nbt(data:('Requests.' + lower(p~'name')));
    if(requests, return(requests), return(null));
);


__place_request(p, target)-> (
    data = __init_nbt(p);
    requests = parse_nbt(data:('Requests.' + lower(p~'name')));
    requests += lower(target);
    data:('Requests.' + lower(p~'name')) = requests;
    store_app_data(data);
    safe_print(target, format('c ' + p~'name') + ' has sent a teleport request. \n' + format('be ACCEPT', '^i Accept Request', '!/tpa accept ' + p~'name') + ' | ' + format('br REJECT', '^i Reject Request', '!/tpa reject ' + p~'name'));
);


__remove_requests(p, target)-> (
    data = __init_nbt(p);
    requests = parse_nbt(data:('Requests.' + lower(p)));
    if(target == '.', requests = [], requests = filter(requests, _ != lower(target)));
    data:('Requests.' + lower(p)) = requests;
    store_app_data(data);
);


__do_tp(p, t)-> (
    target = player(t);
    if(target != null, (
        if(__assert_player_can_tp_return(target), (
            run('tp ' + target~'name' + ' ' + p~'name');
            print(p, 'You accepted ' + format('c ' + target~'name' + '\'s request'));
            print(target, 'Your request was accepted by ' + format('c ' + p~'name'));
            __remove_requests(t, p);
            return(null);
        ), print(p, format('r Unable to teleport ' + t + '. They must be on solid ground, not suffocating, in air, and not on fire. Their request has not been removed')));
    ), print(p, format('r Unable to teleport ' + t + ', as they are offline. Their request has not been removed')));
);


__accept_request(p, target)-> (
    if(target == '.', (
        requests = __get_all_requested(p);
        if(requests != null, (
            print(p, requests);
            for(requests, (
                __do_tp(p, _);
            ));
        ), print(p, format('r You have no pending incoming requests')));
    ), (
        if(__check_requests(target, p), (
            __do_tp(p, target);
        ), print(p, format('r You cannot accept a non-existent request')));
    ));
);


__reject(p, target) -> (
    if(__check_requests(target, p), (
        __remove_requests(target, p);
        print(p, 'You rejected ' + format('c ' + target + '\'s') + ' request');
        safe_print(target, 'Your request was rejected by ' + format('c ' + p~'name'));
    ), print(p, format('r You cannot reject a non-existent request')));
);


__reject_request(p, target)-> (
    if(target == '.', (
        requests = __get_all_requested(p);
        if(requests != null, (for(requests, (__reject(p, _);)), print(p, format('r You have no pending incoming requests'))));
    ), (__reject(p, target)));
);


__null_target(p)-> (
    print(p, format('r The specified player was not found'));
    exit();
);


__prevent_self(p, t)-> (
    if(lower(p) == lower(t), (
        print(p, format('r Invalid player'));
        exit(0);
    ));
);


__warp(t)-> (
    p = player();
    __prevent_self(p, t);
    if(!__check_requests(p, t), (
        __place_request(p, t);
        print(p, 'You will be teleported to ' + format('c ' + t) + ' if the request is accepted');
    ), print(p,  'You have already sent a request to ' + format('c ' + t) + '. Click ' + format('bc here', '^i Cancel Request', '!/tpa cancel ' + t) + ' to cancel the request'));
);


__accept_warp(t)-> (
    p = player();
    __prevent_self(p, t);
    if(t == '.', (
        __accept_request(p, t);
        return(null);
    ), (
        target = player(t);
        if(target == null, __null_target(p));
        __accept_request(p, target);
    ));
);


__reject_warp(t)-> (
    p = player();
    __prevent_self(p, t);
    if(t == '.', (
        __reject_request(p, t);
        return(null);
    ), (
        __reject_request(p, t);
    ));
);


__cancel(t)-> (
    p = player();
    __prevent_self(p, t);
    if(t == '.', (
        if(__view_all_requests(p) != null, (
            __remove_requests(p, t);
            print(p, 'All requests have been canceled');
            return(null);
        ), (
            print(p, format('r You have no pending outgoing requests'));
            return(null);
        ));
    ));
    if(__check_requests(p, t), (
        __remove_requests(p, t);
        print(p, 'Your request to ' + format('c ' + t) + ' was successfully canceled');
    ), print(p, format('r You currently do not have an outgoing request for ', 'br ' + t)));
);


__see_pending()-> (
    p = player();
    requests = __view_all_requests(p);
    if(requests != null, (
        print(p, 'You have the following pending outgoing requests:');
        for(requests, print(p, ' - ' + safe_player(_) + ' [' + format('be CANCEL', '^i Cancel Request', '!/tpa cancel ' + _) + ']'));
    ), print(p, 'You have no pending outgoing requests'));

    requested = __get_all_requested(p);
    if(requested != null, (
        print(p, 'You have the following pending incoming requests:');
        for(requested, print(p, ' - ' + safe_player(_) + ' [' + format('be ACCEPT', '^i Accept Request', '!/tpa accept ' + _) + ' | ' + format('br REJECT', '^i Reject Request', '!/tpa reject ' + _) + ']'));
    ), print(p, 'You have no pending incoming requests'));
);