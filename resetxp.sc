__config()-> (
    m(
        l('stay_loaded', 'true'),
        l('scope', 'player'),
    )
);

__command()-> (
    p = player();
    modify(p, 'xp_level', 0);
    modify(p, 'xp_progress', 0);
    modify(p, 'xp_score', 0);
    print(p, 'Your XP has been reset');
    return(null);
);