__config()-> {
    'stay_loaded' -> 'true',
    'scope' -> 'global',
    'command_permission' -> 'ops',
    'commands' -> {
        '' -> '__init_bossbar',
        'cancel' -> '__cancel_restart',
        'delay' -> _() -> print(player(), 'The current delay is ' + global_time + ' seconds'),
        'delay <seconds>' -> '__set_delay',
    },
    'arguments' -> {
        'seconds' -> {
            'type' -> 'int',
            'min' -> 10,
            'max' -> 300,
            'suggest' -> [10, 15, 20, 30, 60],
        },
    },
};

global_time = 10;
global_restart_queued = false;
global_restart_cancel = false;

__cancel_restart()-> (
    global_restart_cancel = true;
    print(player(), 'The restart has been cancelled');
    return(null);
);

__set_delay(amount)-> (
    global_time = amount;
    print(player(), 'The delay has been set to ' + global_time + ' seconds');
    return(null);
);

__stop_server()-> (
    global_restart_queued = false;
    if(!global_restart_cancel, run('execute as PadThai02 run stop'), global_restart_cancel = false);
    return(null);
);

__remove_bossbar()-> (
    bossbar('marwynn:restart', 'visible', false);
    bossbar('marwynn:restart', 'remove');
    __stop_server();
);

__bossbar_time_decr(time)-> (
    bossbar('marwynn:restart', 'name', 'Server Restarting in ' + time + ' ' + if(time == 1, 'second', 'seconds'));
    if(time == 1 || global_restart_cancel, schedule(20, '__remove_bossbar'), schedule(20, '__bossbar_time_decr', time - 1));
);

__bossbar_value_incr(value)-> (
    bossbar('marwynn:restart', 'value', value);
    if(value == bossbar('marwynn:restart', 'max') - 1, return(null), schedule(2, '__bossbar_value_incr', value + 1));
);

__init_bossbar()-> (
    if(global_restart_queued, print(player(), 'A restart is already in progress. Cancel with /restart cancel'));
    global_restart_queued = true;
    bb = bossbar('marwynn:restart');
    bossbar(bb, 'name', 'Server Restarting Soon!');
    bossbar(bb, 'color', 'blue');
    bossbar(bb, 'style', 'progress');
    bossbar(bb, 'max', round(100 * global_time / 10));
    bossbar(bb, 'visible', true);
    bossbar(bb, 'players', player('all'));
    __bossbar_time_decr(global_time);
    __bossbar_value_incr(0);
    return(null);
);