import('lib', '__get_custom_nbt');
__config()-> {
    'stay_loaded' -> 'true',
    'scope' -> 'player'
};

_d = load_app_data();
if(!_d, (
    _d = nbt('{}');
    store_app_data(_d);
));


global_cd_stage = {
    0 -> 20,
    1 -> 15,
    2 -> 10,
};


global_undead_mobs = [
    'bogged',
    'drowned',
    'husk',
    'phantom',
    'skeleton',
    'skeleton_horse',
    'stray',
    'wither',
    'wither_skeleton',
    'zoglin',
    'zombie',
    'zombie_horse',
    'zombie_villager',
    'zombified_piglin'
];


__init_data(p)-> {
    data = load_app_data();
    if(!data:(p~'uuid'), (
        data:(p~'uuid') = -9999;
        store_app_data(data);
    ));
    return(data);
};


__save_cooldown(p, data, _tick_time)-> {
    data:(p~'uuid') = _tick_time;
    store_app_data(data);
};


__get_damage_effect_name(e)-> {
    if(global_undead_mobs~(e~'type'), return('instant_health'), return('instant_damage'));
};


__smite_entity(p, e, stage)-> {
    run('playsound marwynn:smite.proc master @a ' + join(' ', e~'pos') + ' 0.7');
    run('summon minecraft:lightning_bolt ' + join(' ', e~'pos'));
    run('advancement grant @s only marwynn:smite_proc');
    effect_amp_damage = __get_damage_effect_name(e);
    if(stage == 1, (
        modify(e, 'effect', effect_amp_damage, 1, 1, false, false);
    ), stage == 2, (
        modify(e, 'effect', effect_amp_damage, 1, 2, false, false);
        modify(e, 'effect', 'slowness', 100);
        modify(e, 'effect', 'mining_fatigue', 100);
    ));
};


__on_player_attacks_entity(p, e)-> {
    hand = query(p, 'holds', 'mainhand');
    if(hand, (
        [item_name, item_amount, item_nbt] = hand;
        if(__get_custom_nbt(item_nbt, 'smite.is_smite'), (
            data = __init_data(p);
            stage = __get_custom_nbt(item_nbt, 'smite.stage');
            cooldown = (global_cd_stage:stage) * 20;
            last_used_smite = data:(p~'uuid');
            current_tick_time = tick_time();
            if(current_tick_time >= last_used_smite + cooldown, (
                __smite_entity(p, e, stage);
                __save_cooldown(p, data, current_tick_time);
            ), (
                remaining_cd_seconds = str('%.2f seconds', (last_used_smite + cooldown - current_tick_time) / 20);
                display_title(p, 'actionbar', format('r Smite is on cooldown for ' + remaining_cd_seconds));
            ));
        ));
    ));
    return();
};


__on_close()-> {
    p = player();
    data = __init_data(p);
    __save_cooldown(p, data, -9999);
};