import('lib', '__get_debug', '__set_debug');


__config()-> {
    'scope' -> 'player',
    'stay_loaded' -> 'true',
    'commands' -> {
        '' -> '__show_current_value',
        '<bool>' -> '__set_value',
    },
    'arguments' -> {
        'bool' -> {
            'type' -> 'bool',
            'suggest' -> [true, false],
        },
    },
};


__show_current_value()-> (
    print(player(), 'Debug: ' + format('c ' + __get_debug()));
);


__set_value(b)-> (
    __set_debug(b);
    print(player(), 'Set debug to ' + format('c ' + b));
);