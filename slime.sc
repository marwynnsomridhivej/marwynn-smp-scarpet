__config()-> {
    'scope' -> 'player',
    'commands' -> {
        '' -> '__check_if_slime_chunk',
    },
};


__clear_title(p)-> {
    display_title(p, 'clear');
};


__check_if_slime_chunk()-> {
    p = player();
    if(p~'dimension' == 'overworld', (
        if(in_slime_chunk(p~'pos'), (
            display_title(p, 'title', format('e its a slime chunk'));
        ), (
            display_title(p, 'title', format('r WOMP WOMP LMAO'));
        ));
        schedule(110, '__clear_title', p);
    ), (
        print(p, format('ri You must be in the overworld to use this command'))
    ));
};