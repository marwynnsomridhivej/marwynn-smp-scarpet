__config()-> {
    'stay_loaded' -> 'true',
    'scope' -> 'player',
    'commands' -> {
        '' -> '__give_magnet',
    },
};


global_magnet_range = [8, 8, 8];


__motion_vector(p, e)-> (
    amplitude = sqrt(
        (p~'x' - e~'x') ^ 2 +
        (p~'y' - e~'y') ^ 2 +
        (p~'z' - e~'z') ^ 2
    );
    amplitude = max(amplitude, 1);
    return([
        p~'x' - e~'x',
        p~'y' - e~'y' + 1,
        p~'z' - e~'z',
    ] / amplitude);
);



__on_player_uses_item(p, item_tuple, hand)-> (
    [item_name, item_amount, item_nbt] = item_tuple;
    if(item_nbt~'Magnet', (
        items_in_range = entity_area('item', player(), global_magnet_range);
        for(items_in_range, (
            modify(_, 'pickup_delay', 0);
            modify(_, 'motion', __motion_vector(p, _));
        ));
    ));
);


__check_craftable(p)-> (
    hand = p~'holds';
    if(hand != null, (
        [item_name, item_amount, item_nbt] = hand;
        if(item_amount == 1 && item_name == 'stick', (
            if(!item_nbt~'Magnet', (
                iron_ingot_slot = inventory_find(p, 'iron_ingot');
                if(iron_ingot_slot != null, (
                    [_iname, iron_ingot_amount, _inbt] = inventory_get(p, iron_ingot_slot);
                    copper_ingot_slot = inventory_find(p, 'copper_ingot');
                    if(copper_ingot_slot != null, (
                        [_cname, copper_ingot_amount, _cnbt] = inventory_get(p, copper_ingot_slot);
                        return([iron_ingot_slot, iron_ingot_amount, copper_ingot_slot, copper_ingot_amount]);
                    ), print(p, format('r You need at least 1 copper ingot to craft Magnet')));
                ), print(p, format('r You need at least 1 iron ingot to craft Magnet')));
            ), print(p, format('r Magnet is already applied to this stick')));
        ), print(p, format('r You must be holding exactly one stick')));
    ), print(p, format('r You must be holding exactly one stick')));
    return(false);
);


__craft_magnet(p, ii_slot, ii_amount, ci_slot, ci_amount)-> (
    slot = p~'selected_slot';
    [item_name, item_amount, item_nbt] = inventory_get(p, slot);
    if(item_nbt == null, item_nbt = nbt({}));
    item_nbt:'Magnet' = true;
    item_nbt:'display' = nbt('{Name:\'[{"text":"Magnet","italic":false,"color":"aqua"}]\',Lore:[\'[{"text":"Upon use, draws in all items","italic":false}]\',\'[{"text":"within an 8 block radius","italic":false}]\']}');
    item_nbt:'Enchantments' = nbt('[{}]');
    item_nbt:'HideFlags' = 1;
    inventory_set(p, slot, item_amount, item_name, item_nbt);
    inventory_set(p, ii_slot, ii_amount - 1);
    inventory_set(p, ci_slot, ci_amount - 1);
    print(p, 'Magnet has been applied to your stick');
);


__give_magnet()-> (
    p = player();
    craftable = __check_craftable(p);
    if(craftable, (
        __craft_magnet(p, ...craftable);
        run('playsound minecraft:ui.toast.challenge_complete master ' + p~'name');
    ));
);