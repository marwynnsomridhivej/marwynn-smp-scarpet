import('lib', '__apply_damage', '__apply_dd_heal', '__check_for_dd', '__cleanse_damage');


global_gw_duration = 3;


__on_player_deals_damage(p, amount, e)-> (
    item = p~'holds';
    if(item != null, (
        [item_name, item_amount, item_nbt] = item;
        if((item_nbt != null) && item_nbt~'Bloodthirster', (
            gw_applied = (query(p, 'effect', 'poison') != null || query(p, 'effect', 'wither') != null);
            if(gw_applied, grievous_wounds_modifier = 0.6, grievous_wounds_modifier = 1.0);
            heal = grievous_wounds_modifier * (global_vamp_modifier * amount);
            modify(p, 'health', p~'health' + heal);
        ));
    ));
    if(e~'type' != 'player', return(__apply_damage(e, amount, 'player', p)));
);


__on_player_takes_damage(p, amount, source, source_entity)-> (
    if(source_entity != null, (
        chestplate = query(p, 'holds', 'chest');
        if(chestplate != null, (
            [item_name, item_amount, item_nbt] = chestplate;
            if((item_nbt != null) && item_nbt~'Thornmail', (
                modify(source_entity, 'effect', 'poison', global_gw_duration * 20);
                if(source_entity~'type' == 'player', (
                    __apply_damage(source_entity, 4, 'thornmail', p);
                ), (
                    if(4 >= source_entity~'health', run('kill ' + source_entity~'uuid'), modify(source_entity, 'health', source_entity~'health' - 4));
                ));
            ));
        ));
    ));
    return(__apply_damage(p, amount, source, source_entity));
);