__config()-> {
    'stay_loaded' -> 'true',
    'scope' -> 'global',
    'commands' -> {
        '<amount>' -> '__convert_stack',
    },
    'arguments' -> {
        'amount' -> {
            'type' -> 'int',
            'min' -> 0,
            'suggest' -> [69, 420]
        }
    }
};

__convert_stack(amount) -> (
    self = player();
    stacks = floor(amount / 64);
    remainder = amount % 64;
    stack_text = 'stacks';
    if(stacks == 1, stack_text = 'stack');
    print(self, stacks + ' ' + stack_text + if(remainder, ' and ' + remainder, ''));
    return(null);
);