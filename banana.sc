import('lib', '__get_custom_nbt');
__config()-> {
    'stay_loaded' -> 'true',
    'scope' -> 'player',
};


__on_player_placing_block(p, item_tuple, hand, block)-> {
    holds = query(p, 'holds', hand);
    [item_name, item_amount, item_nbt] = holds;
    if(__get_custom_nbt(item_nbt, 'banana') == true, return('cancel'), return());
};