import('lib', '__get_custom_nbt', '__set_notify', '__check_mode_enabled');
import('drill_nbt',
    'global_new_template',
    'global_origin_on',
    'global_origin_off',
    'global_offset_on',
    'global_offset_off',
);
import('offsets', '__generate_offsets');

__config()-> {
    'stay_loaded' -> 'true',
    'scope' -> 'player',
    'commands' -> {
        '' -> '__configure_drill',
    },
};


_d = load_app_data();
if(!_d, (
    _d = nbt('{}');
    store_app_data(_d);
));


global_offsets = __generate_offsets([
    [-1, 1, 0],  [0, 1, 0],  [1, 1, 0],
    [-1, 0, 0],              [1, 0, 0],
    [-1, -1, 0], [0, -1, 0], [1, -1, 0],
]);
global_offset_indices = [0, 1, 2, 3, 5, 6, 7, 8];
global_drillables = [
    'wooden_pickaxe',
    'stone_pickaxe',
    'iron_pickaxe',
    'gold_pickaxe',
    'diamond_pickaxe',
    'netherite_pickaxe',

    'wooden_shovel',
    'stone_shovel',
    'iron_shovel',
    'gold_shovel',
    'diamond_shovel',
    'netherite_shovel',
];


__check_drillable(item_name, item_nbt)-> {
    return(bool(global_drillables~item_name != null || __get_custom_nbt(item_nbt, 'paxels.is_paxel')));
};


__ensure_safe_nbt(p)-> {
    data = load_app_data();
    if(data:(p~'name') == null, (
        put(data, p~'name', nbt(global_new_template));
    ));
    return(data);
};


__on_start()-> {
    p = player();
    data = __ensure_safe_nbt(p);
    __set_notify(p, 'drill', data:(p~'name'):'enabled');
};


__get_offsets(p)-> {
    main_face = query(p, 'facing', 0);
    if(main_face != 'up' && main_face != 'down', (
        currently_facing = main_face;
    ), (
        secondary_face = query(p, 'facing', 1);
        currently_facing = main_face + '-' + secondary_face;
    ));
    return(global_offsets:currently_facing);
};


__break_surroundings(p, block)-> {
    data = __ensure_safe_nbt(p);
    origin = pos(block);
    offsets = __get_offsets(p);
    for(global_offset_indices, (
        if(data:(p~'name'):'offsets':_, (
            loc = origin + offsets:_i;
            if(block(loc) != 'nether_portal' && block(loc) != 'end_portal' && block(loc) != 'end_gateway', harvest(p, origin + offsets:_i));
        ));
    ));
};


__on_player_breaks_block(p, block)-> {
    data = __ensure_safe_nbt(p);
    if(data:(p~'name'):'enabled', (
        hand = query(p, 'holds', 'mainhand');
        if(hand, (
            [item_name, item_count, item_nbt] = hand;
            if(__check_drillable(item_name, item_nbt), (
                if(__check_mode_enabled(p, 'suppressor'), without_updates(__break_surroundings(p, block)), __break_surroundings(p, block);
                );
            ));
        ));
    ));
    return();
};


__callback(screen, p, action, data)-> {
    slot = data:'slot';
    if(action == 'pickup' && slot <= 8, (
        if(slot == 4, (
            __toggle_drill(p, screen);
        ), (
            __toggle_offset(p, slot, screen);
        ));
    ));
    return('cancel');
};


__configure_drill()-> {
    p = player();
    data = __ensure_safe_nbt(p);
    screen = create_screen(p, 'generic_3x3', 'Drill Configuration', '__callback');
    if(data:(p~'name'):'enabled', origin_nbt = global_origin_on, origin_nbt = global_origin_off);
    inventory_set(screen, 4, null, null, origin_nbt);
    for(global_offset_indices, (
        if(data:(p~'name'):'offsets':_, offset_nbt = global_offset_on, offset_nbt = global_offset_off);
        inventory_set(screen, _, null, null, offset_nbt);
    ));
};


__toggle_drill(p, screen)-> {
    data = __ensure_safe_nbt(p);
    status = !data:(p~'name'):'enabled';
    put(data, p~'name' + '.enabled', status);
    inventory_set(screen, 4, null, null, if(status, global_origin_on, global_origin_off));
    store_app_data(data);
    __set_notify(p, 'drill', status);
    return('cancel');
};


__toggle_offset(p, slot, screen)-> {
    data = __ensure_safe_nbt(p);
    if(data:(p~'name'):'offsets':slot, (
        put(data, p~'name' + '.offsets.' + slot, false);
        inventory_set(screen, slot, null, null, global_offset_off);
    ), (
        put(data, p~'name' + '.offsets.' + slot, true);
        inventory_set(screen, slot, null, null, global_offset_on);
    ));
    store_app_data(data);
    return('cancel');
};