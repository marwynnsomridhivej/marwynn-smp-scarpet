# Custom Commands
Below is a complete list of custom commands used on the SMP server. If not otherwise mentioned, all players should have
permission to fully use these commands.

## Coordinate Conversion
Converts between your overworld and nether coordinates.

Usage: `/cc`

## Death Coordinates
Automatically displays the coordinates at which you died upon death.

Usage: `/deathcoords` or automatically upon death

## DM Coordinates
Sends the selected player your coordinates in the current dimension you are in.

Usage: `/dmc <player>`

## Drill Mode
Players can now mine multiple blocks at a time in the direction they are facing
by defining a pattern. Drill mode has only one command, which displays the
configuration GUI. All configuration options persist on logout, so you will not
need to reconfigure your pattern upon every login.

Usage: `drill`

### Drill Mode Config - Origin
The origin is the block players mine. The pattern is mined relative to the origin
in the direction you are facing. Click on the origin block in the configuration
GUI to toggle between enabling and disabling drill mode.
![image](/img/drill%20origin%20active.png)
![image](/img/drill%20origin%20inactive.png)

### Drill Mode Config - Offsets
The offsets are the blocks that define the pattern of blocks that will automatically
be mined in drill mode (relative to the origin and facing). Click on each of the
offset blocks in the configuration GUI to toggle between enabling and disabling
the particular offset.
![image](/img/drill%20offset%20active.png)
![image](/img/drill%20offset%20inactive.png)

### Drill Mode Warning
To aid players in knowing whether or not drill mode is active, if drill mode is
on, the player will receive a brief notification above their hotbar warning them
that drill mode is on upon switching to a valid pickaxe or shovel. This
notification will automatically disappear after a brief period.
![image](/img/drill%20warning.png)

## FF
Go next.

Usage: `/ff`

## Home Teleport
Teleports you back to your bed (if an overworld respawn point is set) or a charged respawn anchor (if a nether respawn
point is set).

Usage: `/home`

Restrictions: You will not be able to teleport if a spawn point is not set in the current dimension, are in the end, in
midair, suffocating, in water, or on fire.

## Reset XP
Resets your XP levels and progress to zero.

Usage: `/resetxp`

## Spectator Mode
Allows the use of spectator mode.

Usage: `/s` both to enter and exit spectator mode

Restrictions: Conditions to enter spectator mode as the same as those for [/home](#home-teleport). Upon exiting spectator
mode, you will be placed back at the location where you entered spectator mode.

## Stack Calculator
Automatically calculates how many stacks a given amount of items would be.

Usage: `/stack [amount (int)]`

Restrictions: Currently only supports calculations for stacks of 64. An implementation of stacks of n will be done soon.

## Teleport
This implementation of teleport includes several commands:

### Request Teleport
Usage: `/tpa <player>`

Restrictions: You may only send up to one request per player.

### Accept Teleport Request
Usage: `/tpa accept <player>` or `/tpa accept .` to accept all pending requests

Restrictions: Only online players will be teleported. Attempts to teleport offline players will fail, but their requests
will be kept as pending. The conditions for teleporting are the same as those for [/home](#home-teleport).

### Reject Teleport Request
Usage: `/tpa reject <player>` or `/tpa reject .` to accept all pending requests

Note: You may reject requests from offline players.

### Cancel Teleport Request
Usage: `/tpa cancel <player>` or `/tpa cancel .` to cancel all pending requests

Note: You may cancel your sent requests to offline players.

### Show Pending Teleport Requests
Usage: `/tpa`

Aliases: `requests` (`/tpa requests`) | `ls` (`/tpa ls`)

# New Mechanics
Due to popular demand and player requests, several new mechanics have been implemented. If listed, all players should
have permission to fully use these mechanics and commands.

## Weapon Augments
Weapons (swords / axes) may be augmented through several commands, adding new unique interactions.

Weapon augments are craftable, but not through the traditional crafting table means. In order to apply an augment to your
weapon, you must be holding the weapon in your mainhand. Additionally, you must have the correct items and amount in your
inventory to satisfy the augment's recipe. The slots those items are in do not matter, so long as you have the required
items and amounts.

Then, upon executing the augment's command, the recipe items will be consumed in their required amounts and the augment
will be applied to your weapon. Any specific augment may only be applied once per weapon, but weapons can have multiple
different augments applied at once. **ONCE YOU APPLY AN AUGMENT, IT IS PERMANENT AND CANNOT BE REMOVED** *(may change 
in the future)*.

### Bloodthirster
**Effect:** heal for 25% of pre-mitigation damage dealt.

**Recipe:**
- 1x sword/axe
- 4x golden apple
- 4x golden carrot

**Command:** `/bloodthirster`

### Infinity Edge
**Effect:** critical strikes do an additional 50% increased damage.

**Recipe:**
- 1x sword/axe
- 64x stone
- 16x iron ingot
- 16x gold ingot
- 4x diamond

**Command:** `/infinityedge`

**Note:** the bonus damage is applied after all vanilla damage modification.

## Armor Augments
Armor (helmets / chestplates / leggings / boots) may be augmented through several commands, adding new unique interactions.

Like weapon augments, armor augments are craftable through commands. In order to apply an augment to your armor, you must
be wearing that armor piece in the correct slot. Like weapon augments, you must have enough of the correct items in your
inventory to satisfy the augment's recipe.

You use commands the same way as with weapon augments. Again, **ONCE YOU APPLY AN AUGMENT, IT IS PERMANENT AND CANNOT BE
REMOVED** *(may change in the future)*.

### Death's Dance
**Effect:** for every instance of damage normally received, take 50% of that immediately, and the remaining 50% is taken
as DOT (damage over time) over 5 seconds. Scoring a kill on a player or hostile mob stops the DOT and heals you for 2.5
HP (hostile mob killed) or 5 HP (player killed).

**Recipe:**
- 1x chestplate
- 1x shield *(unenchanted)*
- 4x magma cream

**Command:** `/deathsdance`

### Thornmail
**Effect:** when attacked, deal 4 HP damage to the attacker and poison them for 3 seconds.

**Recipe:**
CURRENTLY UNCRAFTABLE

**Command:** CURRENTLY UNCRAFTABLE

**Note:** Repeated attacks within the poison duration only refresh the poison duration. Thornmail will be made craftable
soon.

## Item Magnet Augment
Tired of running around to collect drops on the ground after mining a huge area out? With the item magnet, right clicking
automatically draws in items within an 8 block spherical radius (but not through solid walls). This stick augment is
crafted in a similar manner to weapon/armor augments, and may only be applied once per stick.

**Recipe:**
- 1x stick *(must be held in mainhand, must have only 1 in the slot)*
- 1x copper ingot
- 1x iron ingot

**Command:** `/magnet`

## Smite Augment
This lightning rod augment summons a lightning bolt from the heavens to strike the target you attacked. This augment is
crafted in a similar manner to weapon/armor augments, and may only be applied once per lightning rod.

**Recipe:** CURRENTLY UNCRAFTABLE

**Command:** CURRENTLY UNCRAFTABLE

**Note:** The is a configurable cooldown between subsequent smite uses. The cooldown is 15 seconds by default, and can
be adjusted (requires operator privileges). Smite will be made craftable soon.

# Commands / Mechanics in Development
N/A, suggestions open.