__config()-> {
    'stay_loaded' -> 'true',
    'scope' -> 'global',
    'commands' -> {
        '<player>' -> '__send_coords',
    },
    'arguments' -> {
        'player' -> {
            'type' -> 'players'
        }
    }
};

__send_coords(target) -> (
    self = player();
    coords = self ~ 'pos';
    print(self, self ~ 'name' + ' is at ' + str(' %d %d %d', coords));
    print(target, self ~ 'name' + ' is at ' + str(' %d %d %d', coords));
    return(null);
);