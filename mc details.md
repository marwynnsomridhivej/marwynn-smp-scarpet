# Minecraft Server Info
- Game Version: `1.21.3`
- IP: `mc.marwynn.me`
- Interactive Map: [Click here!](https://map.marwynn.me)

You can join the world using the vanilla `1.21.3` client. **NO ADDITIONAL MODS
ARE NECESSARY**. Any mods and datapacks mentioned below are server side only
unless otherwise specified.

The server requires a resource pack in order for some of the textures to 
appear correctly. The game will automatically download the resource pack for you
upon connecting to the server. If for some reason this does not work,
[click here](https://www.dropbox.com/scl/fi/olh4idttx9u8nzso7m70u/welded-pack.zip?rlkey=xt30nd0rzqcvfhs7wltd7s695&st=ave85ebl&dl=1)
to download the resource pack manually.

Players are visible on the interactive map unless they are 


## Mods and Datapacks

### External
- [Alternate Current](https://modrinth.com/mod/alternate-current)
- [Amplified Nether](https://modrinth.com/datapack/amplified-nether)
- [Amplified Nether Height](https://modrinth.com/datapack/amplified-nether-height)
- [Armor Stand Editor](https://modrinth.com/mod/armorstandeditor)
- [Audaki Cart Engine (Faster Minecarts)](https://modrinth.com/mod/audaki-cart-engine/versions)
- [Backpacks](https://modrinth.com/datapack/vanilla-backpacks)
- [BlueMap](https://map.marwynn.me)
- [Bucketable](https://modrinth.com/datapack/bucketable)
- [Graves](https://modrinth.com/mod/universal-graves)
- [Josh's More Foods](https://modrinth.com/datapack/joshs-more-foods)
- [NoExpensive](https://www.curseforge.com/minecraft/mc-mods/noexpensive)
- [No Feather Trample](https://modrinth.com/mod/no-feather-trample)
- [Ore Harvester](https://modrinth.com/mod/ore-harvester)
- [Paxels](https://modrinth.com/datapack/paxels/)
- [qraftyfied: STRUCTURES](https://modrinth.com/datapack/qraftyfied)
- [Tectonic](https://modrinth.com/datapack/tectonic)
- [Terralith](https://modrinth.com/datapack/terralith)
- [Tree Harvester](https://modrinth.com/mod/tree-harvester)

### Custom Coded
- [Backpack](/dp-rp/Backpack/backpack.md)
- [Banana](/dp-rp/Banana/banana.md)
- [CustomMusicDiscs](/dp-rp/CustomMusicDiscs/custommusicdiscs.md)
- [ExtendedEnchantments](/dp-rp/ExtendedEnchantments/extendedenchantments.md)
- [Heartsteel](/dp-rp/Heartsteel/heartsteel.md)
- [LowTierWarden](/dp-rp/LowTierWarden/lowtierwarden.md)
- [NotchApple](/dp-rp/NotchApple/notchapple.md)
- [Oceanlord's Decree](/dp-rp/OceanlordsDecree/oceanlordsdecree.md)
- [Smite](/dp-rp/Smite/smite.md)

## Custom Commands
More detailed usage information and code for these commands can be found in
[this Gitlab repo](https://gitlab.com/marwynnsomridhivej/marwynn-smp-scarpet). 
**ONLY THE ONES I LISTED BELOW HERE ARE ACTUALLY USED ON THIS SERVER.**
Please let me know if anything does not work as you expect it to.

### Convert Overworld and Nether Coordinates
Self-explanatory. Not usable in the end.
- Usage: `/cc`

### Convert XP into Bottles o' Enchanting
Allows you to convert your hotbar XP into XP bottles so you can repair tools on
demand. Conversion occurs at a rate of 7XP per bottle.
- Usage: `/convertxp <amount>`

### DM Coordinates to Other Player
I hate having to F3 and typing coords to share. You probably do too. This command
will send a private message to the desired player with your current coordiantes.
- Usage: `/dmc <player>`

### Deathboard Visibility Toggle
Want to make fun at someone for dying a bunch of times but don't want the deaths
sidebar to take up half your screen all the time? You can now toggle the
visibility on and off.
- Toggle ON: `/deathboard enable`, `/deathboard on`
- Toggle OFF: `/deathboard disable`, `/deathboard off`

### Drill Mode
Players can now mine multiple blocks at a time in the direction they are facing
by defining a pattern. Drill mode has only one command, which displays the
configuration GUI. All configuration options persist on logout, so you will not
need to reconfigure your pattern upon every login.
- Usage: `/drill`

#### Drill Mode Config - Origin
>The origin is the block players mine. The pattern is mined relative to the origin
in the direction you are facing. Click on the origin block in the configuration
GUI to toggle between enabling and disabling drill mode.
![image](/img/drill%20origin%20active.png)
![image](/img/drill%20origin%20inactive.png)

#### Drill Mode Config - Offsets
>The offsets are the blocks that define the pattern of blocks that will automatically
be mined in drill mode (relative to the origin and facing). Click on each of the
offset blocks in the configuration GUI to toggle between enabling and disabling
the particular offset.
![image](/img/drill%20offset%20active.png)
![image](/img/drill%20offset%20inactive.png)

#### Drill Mode Warning
>To aid players in knowing whether or not drill mode is active, if drill mode is
on, the player will receive a brief notification above their hotbar warning them
that drill mode is on upon switching to a valid pickaxe or shovel. This
notification will automatically disappear after a brief period.
![image](/img/drill%20warning.png)

### Placement Mode
The placement analog to drill mode. Likewise, it also has a single command, which
displays the configuration GUI. All configuration options persist on logout, so
you will not need to reconfigure your pattern upon every login.
- Usage: `/placement`

#### Placement GUI
This GUI is straightforward and simple to use. Just pick the block from your
inventory and select any offset to set it to your selected block.

Notable changes from the drill mode GUI are:
1. The offset configuratble area is a 5x5 centered around the origin
2. Reset button (blue looping arrow) allows you to quickly inactivate all offsets
3. Set all button (command block) allows you to set all
4. Toggle replacing fluids (by default, placement will not replace fluids)
5. Toggle real-time placement preview rendering

To use the set all button, click the command block while holding onto the
desired block. All offsets will be set to the selected block.

#### Placement Mode Warning
>To aid players in knowing whether or not placement mode is active, the player
will receive a brief notification above their hotbar warning them that placement
mode is on when they switch their hotbar slot to any placeable block. This
notification will automatically disappear after a brief period.

### Suppressor Mode
Allows for breaking blocks without triggering any block update events. It is also
fully compatible with drill mode.
- Toggle ON: `/suppressor on` or `/suppressor enable`
- Toggle OFF: `/suppressor off` or `/suppressor disable`

### Home TP
TP back to your bed (overworld) or respawn anchor (nether). Your bed must be
unobstructed (2 blocks of empty space above), and the respawn anchor must have
at least 1 charge level, otherwise the teleport will fail.
- Usage: `/home`

### Spectator Camera
Enter or exit spectator mode.
- Usage: `/s`

### Calculate Stack
How many stacks is *n* items?
- Usage: `/stack <amount>`

### Slime Chunks
Tells you if the current chunk you're in is a slime chunk.
- Usage: `/slime`

### Teleport to Player
Essentially my implementation of TPA. In order to teleport, you must request to 
teleport to another player. Should they accept, you will be teleported to their
current location. If you are on the receiving end, a private message will appear
with clickable options to accept or reject the incoming request.
- Request to TP: `/tpa <player>`
- Accept TPA: `/tpa accept <player>`
- Reject TPA: `/tpa reject <player>`
- Cancel TPA: `/tpa cancel <player>`
    - Cancels YOUR outgoing TPA request to the specified player
- See Pending TPA Requests: `/tpa requests`
    - Aliases: `/tpa ls`, `/tpa`


## Reporting Issues
I've installed a handful of server-side optimisation mods that hopefully should
improve performance of the server, especially at higher player counts. However,
should you experience any lag on your end, make sure that the lag is not due to
your own system being unable to display frames or process chunks fast enough for
your desired framerate (FPS lag). If this is the case, try lowering your graphics
settings.

I've provided everyone with a way of checking the server's TPS (ticks per second)
and MSPT (milliseconds per tick). These two metrics are tied to each other. The
game runs at `20TPS` normally, which gives the server `50ms` of time to process ticks
before it starts to lag. If TPS drops below 20 (MSPT > 50), you can be pretty
confident the lag is server-side, so please message me then. The more detailed
the better I can troubleshoot the issue and hopefully resolve it.

Also, if for some reason the server suddenly dies or you are not able to connect,
please let me know and I will try to resolve it ASAP.


## Optimisation
- [C2ME](https://modrinth.com/mod/c2me-fabric)
- [Chunky](https://modrinth.com/plugin/chunky)
- [Carpet](https://github.com/gnembon/fabric-carpet)
- [Disable Portal Checks](https://modrinth.com/mod/disable-portal-checks)
- [FerriteCore](https://modrinth.com/mod/ferrite-core)
- [Krypton](https://modrinth.com/mod/krypton)
- [Lithium](https://modrinth.com/mod/lithium)
- [ModernFix](https://modrinth.com/mod/modernfix)
- [Noisium](https://modrinth.com/mod/noisium)
- [RecipeCooldown](https://modrinth.com/mod/recipecooldown)
- [Spark](https://modrinth.com/mod/spark)
- [Very Many Players](https://modrinth.com/mod/vmp-fabric)


# Contact
Message me on Discord (`@msarranges`)