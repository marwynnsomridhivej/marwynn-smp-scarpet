import('lib', 'global_allowed_weapons', '__apply_only_lore');


__config()-> {
    'stay_loaded' -> 'true',
    'scope' -> 'player',
    'commands' -> {
        '' -> '__give_bloodthirster',
    },
};


__check_craftable(p)-> (
    hand = p~'holds';
    if(hand != null, (
        [weapon_name, _wamount, weapon_nbt] = hand;
        if(global_allowed_weapons~weapon_name != null, (
            if(!weapon_nbt~'Bloodthirster', (
                golden_apple_slot = inventory_find(p, 'golden_apple');
                if(golden_apple_slot != null, (
                    [_gname, golden_apple_amount, _gnbt] = inventory_get(p, golden_apple_slot);
                    if(golden_apple_amount >= 4, (
                        golden_carrot_slot = inventory_find(p, 'golden_carrot');
                        if(golden_carrot_slot != null, (
                            [_gname, golden_carrot_amount, _gnbt] = inventory_get(p, golden_carrot_slot);
                            if(golden_carrot_amount >= 4, (
                                return([golden_apple_slot, golden_apple_amount, golden_carrot_slot, golden_carrot_amount]);
                            ), print(p, format('r You need at least 4 golden carrots to craft Bloodthirster')));
                        ), print(p, format('r You need at least 4 golden carrots to craft Bloodthirster')));
                    ), print(p, format('r You need at least 4 golden apples to craft Bloodthirster')));
                ), print(p, format('r You need at least 4 golden apples to craft Bloodthirster'))); 
            ), print(p, format('r Bloodthirster has already been applied to your mainhand weapon')));
        ), print(p, format('r Bloodthirster cannot be applied to your mainhand item')));
    ), print(p, format('r You must have a weapon equipped on your mainhand')));
    return(false);
);


__craft_bloodthirster(p, golden_apple_slot, golden_apple_amount, golden_carrot_slot, golden_carrot_amount)-> (
    weapon_slot = p~'selected_slot';
    [item_name, item_amount, item_nbt] = inventory_get(p, weapon_slot);
    item_nbt:'Bloodthirster' = true;
    new_nbt = __apply_only_lore(item_nbt, [
        '[{"text":"Bloodthirster Applied:","italic":false,"color":"dark_red","underlined":true},{"text":"","italic":false,"color":"dark_purple","underlined":false}]',
        '[{"text":"Heal for ","italic":false,"color":"dark_red"},{"text":"25%","color":"red"},{"text":" of the damage dealt","color":"dark_red"}]',
        '[{"text":"by this weapon","italic":false,"color":"dark_red"}]',
    ]);
    inventory_set(p, weapon_slot, item_amount, item_name, new_nbt);
    inventory_set(p, golden_apple_slot, golden_apple_amount - 4);
    inventory_set(p, golden_carrot_slot, golden_carrot_amount - 4);
    print(p, 'Bloodthirster has been applied to your mainhand weapon');
);


__give_bloodthirster()-> (
    p = player();
    craftable = __check_craftable(p);
    if(craftable, (
        __craft_bloodthirster(p, ...craftable);
        run('playsound minecraft:ui.toast.challenge_complete master ' + p~'name');
    ));
);