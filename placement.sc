import('lib', '__set_notify');
import('offsets', '__generate_offsets');
import(
    'placement_nbt',
    'global_new_template',
    'global_preview_modifier',
    '__side_block',
    '__origin_block',
    '__fluids_block',
    '__preview_block',
    '__offset_block',
);
__config() -> {
    'stay_loaded' -> true,
    'scope' -> 'player',
    'commands' -> {
        '' -> '__configure_placement',
    },
};


_d = load_app_data();
if(!_d, (
    _d = nbt('{}');
    store_app_data(_d);
));


global_offsets = __generate_offsets([
    [ -2,  2,  0], [ -1,  2,  0], [ 0,  2,  0], [ 1,  2,  0], [ 2,  2,  0],
    [ -2,  1,  0], [ -1,  1,  0], [ 0,  1,  0], [ 1,  1,  0], [ 2,  1,  0],
    [ -2,  0,  0], [ -1,  0,  0],               [ 1,  0,  0], [ 2,  0,  0],
    [ -2, -1,  0], [ -1, -1,  0], [ 0, -1,  0], [ 1, -1,  0], [ 2, -1,  0],
    [ -2, -2,  0], [ -1, -2,  0], [ 0, -2,  0], [ 1, -2,  0], [ 2, -2,  0],
]);
global_offset_indices = [
     2,  3,  4,  5,  6,
    11, 12, 13, 14, 15,
    20, 21,   , 23, 24,
    29, 30, 31, 32, 33,
    38, 39, 40, 41, 42,
];
global_none_indices = [
     0,  1,  7,  8,
        10, 16,         // 9  --> reset placement
    18, 19, 25, 26,     // 17 --> set all
        28, 34,         // 27 --> replace fluids
    36, 37, 43, 44,     // 35 --> preview
];
global_invalid_blocks = [
    'barrier',
    'structure_void',
    'structure_block',
    'light',
    'command_block',
    'chain_command_block',
    'repeating_command_block',
    'jigsaw',
    'bedrock',
    'air',
    'water',
    'lava',
];
global_inventory_state = {};
global_selected_inventory_block = null;
global_replace_fluids = false;
global_show_preview = false;
global_do_placement = false;
global_inventory_snapshot = {};
global_placement_snapshot = {};


__ensure_safe_nbt(p)-> {
    data = load_app_data();
    if(data:(p~'uuid') == null, (
        put(data, p~'uuid', nbt(global_new_template));
    ));
    return(data);
};


__on_start()-> {
    p = player();
    data = __ensure_safe_nbt(p);
    global_show_preview = data:(p~'uuid'):'show_preview';
    global_replace_fluids = data:(p~'uuid'):'replace_fluids';
    global_do_placement = data:(p~'uuid'):'enabled';
    for(global_offset_indices, (
        _block = data:(p~'uuid'):'offsets':_;
        if(_block != 'none', (
            if(global_placement_snapshot:_block == null,
                global_placement_snapshot:_block = 1,
                global_placement_snapshot:_block += 1
            );
        ));
    ));
    __set_notify(p, 'placement', global_do_placement);
    handle_event('tick', '__show_preview', p);
};


__on_player_connects(p) -> {
    handle_event('tick', '__show_preview', p);
};


__is_placeable(item_name)-> {
    try(return(block(item_name)), 'unknown_block', return(false));
};


__get_active_placeable_item(p)-> {
    holding = query(p, 'holds', 'mainhand');
    if(!(holding && __is_placeable(holding:0)), holding = query(p, 'holds', 'offhand'));
    if(holding, (
        [item_name, item_amount, item_nbt] = holding;
        if(__is_placeable(item_name), return(item_name), return(null));
    ), return(null));
};


__get_player_facing(p)-> {
    facing = query(p, 'facing', 0);
    if(['up', 'down']~facing != null, facing += '-' + query(p, 'facing', 1));
    return(facing);
};


__get_blocks_and_pos(p, custom_origin)-> {
    active_item = __get_active_placeable_item(p);
    if(active_item, (
        interaction_range = query(p, 'attribute', 'block_interaction_range');
        origin = if(custom_origin, custom_origin, pos(query(player(), 'trace', interaction_range, 'blocks')));
        facing = __get_player_facing(p);
        data = __ensure_safe_nbt(p);
        offsets = data:(p~'uuid'):'offsets';
        indices = filter(global_offset_indices, offsets:_ != 'none');
        return(map(indices, [global_offsets:facing:(global_offset_indices~_) + origin, offsets:_]));
    ), return(null));
};


__init_global_inventory_snapshot(p)-> {
    global_inventory_snapshot = {};
    for(range(inventory_size(p)), (
        slot = inventory_get(p, _);
        if(slot != null, (
            [name, amount, nbt] = slot;
            if(global_inventory_snapshot:name == null, global_inventory_snapshot:name = amount, global_inventory_snapshot:name += amount);
        ));
    ));
};


__has_enough(p, held_item_tuple)-> {
    if(p~'gamemode' == 'creative', return(true));
    __init_global_inventory_snapshot(p);
    held_item_name = held_item_tuple:0;
    check = all(pairs(global_placement_snapshot), (
        [item_name, placement_amount] = _;
        inventory_amount = global_inventory_snapshot:item_name;
        if(held_item_name == item_name, inventory_amount = inventory_amount - 1);
        inventory_amount >= placement_amount;
    ));
    return(check);
};


__on_player_places_block(p, item_tuple, hand, _block) -> {
    if(global_do_placement, (
        if(__has_enough(p, item_tuple), (
            blocks_pos = __get_blocks_and_pos(p, null);
            facing = __get_player_facing(p);
            failed = 0;
            for(blocks_pos, (
                [_pos_triple, block_name] = _;
                if(air(_pos_triple) || (global_replace_fluids && liquid(_pos_triple)), (
                    place_item(block_name, ..._pos_triple, facing, p~'sneaking');
                    inventory_remove(p, block_name, 1);
                ),
                failed += 1));
            );
            if(failed > 0, display_title(p, 'actionbar', format('r Failed to place ' + failed + if(failed != 1, ' blocks', ' block'))))
        ), display_title(p, 'actionbar', format('r Insufficient blocks for the defined pattern')));
    ));
    return(null);
};


__draw_preview(p, origin)-> {
    facing = p~'facing';
    if(['up', 'down']~facing != null, facing = query(p, 'facing', 1));
    draw_shape('block', 2, {
        'pos' -> origin,
        'block' -> __get_active_placeable_item(p),
        'facing' -> facing,
        'player' -> p,
    });
    for(__get_blocks_and_pos(p, origin), (
        [_pos_triple, block_name] = _;
        draw_shape('block', 2, {
            'pos' -> _pos_triple,
            'block' -> block_name,
            'facing' -> facing,
            'player' -> p,
        });
    ));
};


__show_preview(p)-> {
    if(!(tick_time() % 2), (
        if(global_do_placement && global_show_preview && __get_active_placeable_item(p), (
            interaction_range = query(p, 'attribute', 'block_interaction_range');
            trace = query(p, 'trace', interaction_range, 'blocks');
            if(trace, (
                block_pos = pos(query(p, 'trace', interaction_range, 'blocks'));
                preview_origin = query(p, 'trace', interaction_range, 'blocks', 'exact');
                diff = preview_origin - block_pos;
                corrected = map(preview_origin, if(diff:_i == 0, _ - 1, (
                    if(diff:_i % 0.0625 == 0, (
                        block_pos:_i + if(p~'pos':_i - block_pos:_i >= 0, 1, -1);
                    ), floor(_));
                )));
                __draw_preview(p, corrected);
            ));
        ));
    ));
    return(null);
};


__callback(screen, p, action, data)-> {
    slot = data:'slot';
    stack = data:'stack';
    if(action == 'slot_update' && slot >= 45, (
        if(stack != null, (
            global_inventory_state:slot = get(stack, 0);
            global_selected_inventory_block = null;
        ), (
            candidate = global_inventory_state:slot;
            try((
                if(block(candidate) && global_invalid_blocks~candidate == null, (
                    global_selected_inventory_block = candidate;
                    delete(global_inventory_state, slot);
                ));
            ), 'unknown_block', null);
        ));
    ), action == 'pickup' && slot < 45, (
        if(slot == 22, (
            __toggle_placement(p, screen);
        ), slot == 9, (
            __reset_placement(p, screen);
        ), slot == 17 && global_selected_inventory_block, (
            __set_all_to_selected(p, screen);
        ), slot == 27, (
            __toggle_fluids(p, screen);
        ), slot == 35, (
            __toggle_preview(p, screen);
        ),global_offset_indices~slot != null, (
            __toggle_offset(p, slot, screen);
        ));
    ), action == 'close', (
        global_inventory_state = {};
        global_selected_inventory_block = null;
    ));
    if(slot < 45, return('cancel'));
};


__configure_placement()-> {
    p = player();
    data = __ensure_safe_nbt(p);
    screen = create_screen(p, 'generic_9x5', 'Placement Configuration', '__callback');
    inventory_set(screen, 17, null, null, __side_block('set-all'));
    inventory_set(screen , 9, null, null, __side_block('retry'));
    inventory_set(screen , 22, null, null, __origin_block(data:(p~'uuid'):'enabled'));
    inventory_set(screen , 27, null, null, __fluids_block(data:(p~'uuid'):'replace_fluids'));
    inventory_set(screen, 35, null, null, __preview_block(data:(p~'uuid'):'show_preview'));
    for(global_none_indices, (
        inventory_set(screen, _, null, null, __side_block(null));
    ));
    for(global_offset_indices, (
        block_id = data:(p~'uuid'):'offsets':_;
        _type = if(block_id != 'none', 'on', 'off');
        inventory_set(screen, _, null, null, __offset_block(block_id, _type));
    ));
};


__toggle_placement(p, screen)-> {
    data = __ensure_safe_nbt(p);
    status = !(data:(p~'uuid'):'enabled');
    put(data, p~'uuid' + '.enabled', status);
    inventory_set(screen, 22, null, null, __origin_block(status));
    store_app_data(data);
    global_do_placement = status;
    __set_notify(p, 'placement', global_do_placement);
};

__toggle_fluids(p, screen)-> {
    data = __ensure_safe_nbt(p);
    status = !(data:(p~'uuid'):'replace_fluids');
    put(data, p~'uuid' + '.replace_fluids', status);
    inventory_set(screen, 27, null, null, __fluids_block(status));
    store_app_data(data);
    global_replace_fluids = status;
};


__toggle_preview(p, screen)-> {
    data = __ensure_safe_nbt(p);
    status = !(data:(p~'uuid'):'show_preview');
    put(data, p~'uuid' + '.show_preview', status);
    inventory_set(screen, 35, null, null, __preview_block(status));
    store_app_data(data);
    global_show_preview = status;
};


__toggle_offset(p, slot, screen)-> {
    data = __ensure_safe_nbt(p);
    previous_item_name = data:(p~'uuid'):'offsets':slot;
    status = if(previous_item_name != 'none', (
        if(previous_item_name != global_selected_inventory_block, global_selected_inventory_block, 'none');
    ), global_selected_inventory_block);
    if(status != null || (status == null && previous_item_name != 'none'), (
        put(data, p~'uuid' + '.offsets.' + slot, if(status != null, status, 'none'));
        _type = if(status != 'none' && status != null, 'on', 'off');
        inventory_set(screen, slot, null, null, __offset_block(global_selected_inventory_block, _type));
        store_app_data(data);

        if(status != 'none' && status != null, global_placement_snapshot:status += 1);
        if(previous_item_name != 'none', global_placement_snapshot:previous_item_name = global_placement_snapshot:previous_item_name - 1);
        if(global_placement_snapshot:previous_item_name == 0, delete(global_placement_snapshot, previous_item_name));
    ));
};


__reset_placement(p, screen)-> {
    data = __ensure_safe_nbt(p);
    for(global_offset_indices, (
        put(data, p~'uuid' + '.offsets.' + _, 'none');
        inventory_set(screen, _, null, null, __offset_block(null, 'off'));
    ));
    store_app_data(data);
    global_placement_snapshot = {};
};


__set_all_to_selected(p, screen)-> {
    if(global_selected_inventory_block, (
        data = __ensure_safe_nbt(p);
        for(global_offset_indices, (
            put(data, p~'uuid' + '.offsets.' + _, global_selected_inventory_block);
            inventory_set(screen, _, null, null, __offset_block(global_selected_inventory_block, 'on'));
        ));
        store_app_data(data);
        global_placement_snapshot = {global_selected_inventory_block: 24};
    ));
};