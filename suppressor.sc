import('lib', '__set_notify');
__config()-> {
    'scope' -> 'player',
    'stay_loaded' -> true,
    'commands' -> {
        '' -> '__status',
        'on' -> '__enable_suppressor',
        'enable' -> '__enable_suppressor',
        'off' -> '__disable_suppressor',
        'disable' -> '__disable_suppressor',
    },
};


global_enabled = false;


__on_start()-> {
    global_enabled = false;
    __set_notify(player(), 'suppressor', global_enabled);
};


__on_player_breaks_block(p, block)-> {
    if(global_enabled, (
        without_updates(harvest(p, pos(block)));
    ));
    return(null);
};


__status()-> {
    p = player();
    print(p, 'Block breaking update suppression is currently ' + if(global_enabled, format('c ENABLED'), format('c DISABLED')));
};


__enable_suppressor()-> {
    p = player();
    if(!global_enabled, (
        global_enabled = true;
        __set_notify(p, 'suppressor', global_enabled);
        print(p, 'Block breaking update supression ' + format('e ENABLED'));
    ), print(p, format('r Block breaking update suppression is already enabled')));
};


__disable_suppressor()-> {
    p = player();
    if(global_enabled, (
        global_enabled = false;
        __set_notify(p, 'suppressor', global_enabled);
        print(p, 'Block breaking update supression ' + format('r DISABLED'));
    ), print(p, format('r Block breaking update suppression is already disabled')));
};