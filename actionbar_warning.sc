__config() -> {
    'stay_loaded' -> true,
    'scope' -> 'player',
};


global_apps = [
    'drill',
    'placement',
    'suppressor',
    'tool ping'
];
global_color_code = {
    'drill'-> 'y',
    'placement'-> 'c',
    'suppressor'-> 'p',
    'tool ping' -> 'd',
};


__apply_formatting(code, text)-> {
    return(format(code + ' ' + text));
};


__display_notification(p)-> {
    need_to_notify = map(filter(global_apps, system_variable_get(p~'uuid' + '.' + _, false)), _);
    if(need_to_notify, (
        _title = format('br CAUTION!') + ' ';
        for(need_to_notify, (
            if(_i > 0, _title += ', ');
            _title += format(global_color_code:_ + ' ' + title(_ + ' mode'));
        ));
        _title += if(length(need_to_notify) != 1, ' are ', ' is ') + format('e ACTIVE');
        display_title(p, 'actionbar', _title);
    ));
    return(null);
};


__on_player_switches_slot(p, from, to) -> {
    __display_notification(p);
};


__on_player_connects(p) -> (
    __display_notification(p);
);