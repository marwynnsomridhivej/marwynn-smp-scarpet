import('lib', 'global_allowed_weapons', '__apply_damage', '__apply_only_lore', '__can_crit');


__config()-> {
    'stay_loaded' -> 'true',
    'scope' -> 'player',
    'commands' -> {
        '' -> '__give_ie',
    },
};


global_stone_msg = format('r You need at least 64 stone to craft Infinity Edge');
global_iron_msg = format('r You need at least 16 iron to craft Infinity Edge');


__craft_ie(p, ss, sa, is, ia, gs, ga, ds, da)-> (
    weapon_slot = p~'selected_slot';
    [item_name, item_amount, item_nbt] = inventory_get(p, weapon_slot);
    item_nbt:'InfinityEdge' = true;
    new_nbt = __apply_only_lore(item_nbt, [
        '[{"text":"Infinity Edge Applied:","italic":false,"color":"#c2ba1b","underlined":true},{"text":"","italic":false,"color":"#c2ba1b","underlined":false}]',
        '[{"text":"Damage from all critical","italic":false,"color":"#c2ba1b"}]',
        '[{"text":"strikes dealt by this weapon","italic":false,"color":"#c2ba1b"}]',
        '[{"text":"are increased by ","italic":false,"color":"#c2ba1b"},{"text":"50%","color":"yellow"}]',
    ]);
    inventory_set(p, weapon_slot, item_amount, item_name, new_nbt);
    inventory_set(p, ss, sa - 64);
    inventory_set(p, is, ia - 16);
    inventory_set(p, gs, ga - 16);
    inventory_set(p, ds, da - 4);
    print(p, 'Infinity Edge has been applied to your mainhand weapon');
);


__check_craftable(p)-> (
    hand = p~'holds';
    if(hand != null, (
        [weapon_name, _wamount, weapon_nbt] = hand;
        if(global_allowed_weapons~weapon_name != null, (
            if(!weapon_nbt~'InfinityEdge', (
                stone_slot = inventory_find(p, 'stone');
                if(stone_slot != null, (
                    [_sname, stone_amount, _snbt] = inventory_get(p, stone_slot);
                    if(stone_amount >= 64, (
                        iron_slot = inventory_find(p, 'iron_ingot');
                        if(iron_slot != null, (
                            [_iname, iron_amount, _inbt] = inventory_get(p, iron_slot);
                            if(iron_amount >= 16, (
                                gold_slot = inventory_find(p, 'gold_ingot');
                                if(gold_slot != null, (
                                    [_gname, gold_amount, _gnbt] = inventory_get(p, gold_slot);
                                    if(gold_amount >= 16, (
                                        diamond_slot = inventory_find(p, 'diamond');
                                        if(diamond_slot != null, (
                                            [_dname, diamond_amount, _dnbt] = inventory_get(p, diamond_slot);
                                            if(diamond_amount >= 4, (
                                                return([stone_slot, stone_amount, iron_slot, iron_amount, gold_slot, gold_amount, diamond_slot, diamond_amount]);
                                            ), print(p, global_diamond_msg));
                                        ), print(p, global_diamond_msg));
                                    ), print(p, global_gold, msg));
                                ), print(p, global_gold_msg));
                            ), print(p, global_iron_msg));
                        ), print(p, global_iron_msg));
                    ), print(p, global_stone_msg));
                ), print(p, global_stone_msg));
            ), print(p, format('r Infinity Edge has already been applied to your mainhand weapon')));
        ), print(p, format('r Infinity Edge cannot be applied to your mainhand item')));
    ), print(p, format('r You must have a weapon equipped on your mainhand')));
    return(false);
);


__give_ie()-> (
    p = player();
    craftable = __check_craftable(p);
    if(craftable, (
        __craft_ie(p, ...craftable);
        run('playsound minecraft:ui.toast.challenge_complete master ' + p~'name');
    ));
);