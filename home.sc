import('lib', '__assert_player_can_tp');


__config()-> (
    m(
        l('stay_loaded', 'true'),
        l('scope', 'player'),
    )
);

__check_unobstructed(coords)-> (
    return(
        (block(coords) == block('air')) ||
        (block(coords) == block('torch')) ||
        (block(coords) == block('wall_torch')) ||
        (block(coords) == block('soul_torch')) ||
        (block(coords)) == block('soul_wall_torch') ||
        (block(coords)) == block('white_wall_banner') ||
        (block(coords)) == block('orange_wall_banner') ||
        (block(coords)) == block('magenta_wall_banner') ||
        (block(coords)) == block('light_blue_wall_banner') ||
        (block(coords)) == block('yellow_wall_banner') ||
        (block(coords)) == block('lime_wall_banner') ||
        (block(coords)) == block('pink_wall_banner') ||
        (block(coords)) == block('gray_wall_banner') ||
        (block(coords)) == block('light_gray_wall_banner') ||
        (block(coords)) == block('cyan_wall_banner') ||
        (block(coords)) == block('purple_wall_banner') ||
        (block(coords)) == block('blue_wall_banner') ||
        (block(coords)) == block('brown_wall_banner') ||
        (block(coords)) == block('green_wall_banner') ||
        (block(coords)) == block('red_wall_banner') ||
        (block(coords)) == block('black_wall_banner')
    );
);

__safe_bed_at_pos(player, coords)-> (
    b = block(coords);
    if(bool(b ~ 'bed'), return(
        above_b1 = block([get(coords, 0), get(coords, 1) + 1, get(coords, 2)]);
        above_b2 = block([get(coords, 0), get(coords, 1) + 2, get(coords, 2)]);
        if(
            !(__check_unobstructed(above_b1) && __check_unobstructed(above_b2)), (
            print(player, 'Your bed is obstructed');
            exit();
        ));
        return(true);
    ));
    print(player, 'A bed at your spawn point is required to teleport home');
    return(false);
);

__safe_anchor_at_pos(player, coords)-> (
    b = block(coords);
    if(bool(b ~ 'respawn_anchor'), return(
        charges = number(block_state(b, 'charges'));
        above_b1 = block([get(coords, 0), get(coords, 1) + 1, get(coords, 2)]);
        above_b2 = block([get(coords, 0), get(coords, 1) + 2, get(coords, 2)]);
        if(
            !(__check_unobstructed(above_b1) && __check_unobstructed(above_b2)), (
                print(player, 'Your respawn anchor is obstructed');
                exit();
            ),
            charges == 0, (
                print(player, 'Your respawn anchor has no charge left');
                exit();
            ),
        );
        set(block(coords), 'respawn_anchor', ['charges', charges - 1]);
        return(true);
    ));
    return(false);
);

__no_spawn(player)-> (
    print(player, 'Please set your spawn point first');
    exit();
);

__command()-> (
    p = player();
    if (p ~ 'dimension' == 'the_end', (
        print(p, 'You must be in either the Overworld or the Nether');
        exit();
    ));
    spawn_point_query = p ~ 'spawn_point';
    actual_coords = get(spawn_point_query, 0);
    dimension = get(spawn_point_query, 1);
    if(spawn_point_query == false, __no_spawn(p));
    if(dimension != p ~ 'dimension', return(
        print(p, 'You may not have a spawn point in the current dimension, or are in a different dimension than the one your spawn is set');
        exit();
    ));
    __assert_player_can_tp(p);
    if((dimension == 'overworld' && __safe_bed_at_pos(p, actual_coords) || (dimension == 'the_nether' && __safe_anchor_at_pos(p, actual_coords))), (
        [tp_x, tp_y, tp_z] = actual_coords;
        if(dimension == 'overworld', tp_y += 0.6, tp_y += 1);
        run('execute in minecraft:' + dimension + ' run tp ' + p ~ 'name' + ' ' + tp_x + ' ' + tp_y + ' ' + tp_z);
    ));
    return(null);
);