import('lib', '__get_custom_nbt');
__config()-> {
    'stay_loaded' -> true,
    'scope' -> 'player',
};


global_mutex = false;


__on_start()->{
    global_mutex = false;
};


__reset_mutex()-> {
    global_mutex = false;
};


__get_backpack_upgrade_level(backpack_nbt)-> {
    return(__get_custom_nbt(backpack_nbt, 'backpack.upgrade_level'));
};


__get_backpack_name(backpack_nbt)-> {
    name = backpack_nbt:'components':'minecraft:custom_name';
    if(!name, (
        name = backpack_nbt:'components':'minecraft:item_name';
    ));
    return(if(name, slice(name, 1, -2), 'Backpack'));
};


__is_backpack(item_tuple)-> {
    [n, a, _nbt] = item_tuple;
    return(__get_backpack_upgrade_level(_nbt) != null);
};


__get_backpack_data(p)-> {
    slot = 'mainhand';
    _maybe_backpack = query(p, 'holds', slot);
    if(_maybe_backpack == null || !__is_backpack(_maybe_backpack), (
        slot = 'offhand';
        _maybe_backpack = query(p, 'holds', slot);
    ));
    if(_maybe_backpack != null, (
        [name, amount, _nbt] = _maybe_backpack;
        backpack_upgrade_level = __get_backpack_upgrade_level(_nbt);
        if(backpack_upgrade_level != null, (
            return([backpack_upgrade_level, if(slot == 'mainhand', p~'selected_slot', -1), __is_ender(_nbt), _nbt]);
        ), return(null));
    ), return(null));
};


__is_ender(backpack_nbt)-> {
    return(__get_custom_nbt(backpack_nbt, 'backpack.ender'));
};


__handle_backpack(screen, p, action, data)-> {
    interacted_with = inventory_get(screen, data:'slot');

    if(action != 'close', (
        if(interacted_with != null, (
            [n, a, _nbt] = interacted_with;
            if(__get_custom_nbt(_nbt, 'backpack.is_backpack'), return('cancel'));
            if(item_tags(n, 'marwynn:backpack/incompatible'), return('cancel'));
        ));
    ));

    if(action == 'close', (
        _maybe_backpack = __get_backpack_data(p);
        if(_maybe_backpack == null, return(null));
        [upgrade_level, slot, is_ender, backpack_nbt] = _maybe_backpack;
        container = [];
        for(range(9 * upgrade_level), (
            _item = inventory_get(screen, _);
            if(_item != null, (
                [item_name, item_amount, item_nbt] = _item;
                if(!is_ender, (
                    put(container, null, {
                        'slot' -> _,
                        'item' -> item_nbt}
                    );
                ), inventory_set('enderchest', p, _, null, null, item_nbt));
            ), is_ender, (
                inventory_set('enderchest', p, _, 0);
            ));
        ));
        if(!is_ender, (
            backpack_nbt:'components.minecraft:container' = container;
            inventory_set(p, slot, null, null, backpack_nbt);
        ));
    ));
    return(null);
};


__do_backpack_logic(p, item_tuple)-> {
    if(item_tuple == null, return(null));
    [item_name, item_amount, item_nbt] = item_tuple;
    if(!__get_custom_nbt(item_nbt, 'backpack.is_backpack'), return(null));
    
    if(global_mutex, return(null));
    global_mutex = true;
    schedule(1, '__reset_mutex');

    _maybe_backpack = __get_backpack_data(p);
    if(_maybe_backpack == null, return(null));

    [upgrade_level, slot, is_ender, backpack_nbt] = _maybe_backpack;
    name = __get_backpack_name(backpack_nbt);
    screen = create_screen(p, 'generic_9x' + upgrade_level, name, '__handle_backpack');
    container = item_nbt:'components.minecraft:container';
    if(container, (
        for(parse_nbt(container), (
            slot = _:'slot';
            _nbt = _:'item';
            inventory_set(screen, slot, null, null, encode_nbt(_nbt));
        ));
    ));
    if(is_ender && inventory_has_items('enderchest', p), (
        for(range(inventory_size('enderchest', p)), (
            maybe_item = inventory_get('enderchest', p, _);
            if(maybe_item, (
                [item_name, item_amount, item_nbt] = maybe_item;
                inventory_set(screen, _, null, null, item_nbt);
            ));
        ));
    ));
    return('cancel');
};


__on_player_uses_item(p, item_tuple, hand)-> {
    return(__do_backpack_logic(p, item_tuple));
};


__on_player_right_clicks_block(p, item_tuple, hand, block, face, hitvec)-> {
    return(__do_backpack_logic(p, item_tuple));
};