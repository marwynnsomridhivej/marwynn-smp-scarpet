# Model Data Index
Custom Model Data numbering scheme: `329XXX` (*numbering starts at 001*)
Use `minecraft:repeating_command_block` for final recipes
1.21.3 introduced the `item_model` component, so these are retained for
backwards compatibility with items that were obtained before the update.


## Banana
|   ID   |               Description                |              Retextured Item              |
| ------ | ---------------------------------------- | ----------------------------------------- |
| 329001 |           Banana item texture            |        `minecraft:structure_block`        |
| 329002 |        Golden Banana item texture        |      `minecraft:chain_command_block`      |
| 329003 |   Enchanted Golden Banana item texture   |    `minecraft:repeating_command_block`    |
| 329004 |        Banana Bundle item texture        |         `minecraft:command_block`         |
| 329005 |     Golden Banana Bundle item texture    |            `minecraft:jigsaw`             |
| 329006 |   Enchanted Banana Bundle item texture   |            `minecraft:barrier`            |


## Backpack
|   ID   |              Description              |           Retextured Item           |
| ------ | ------------------------------------- | ----------------------------------- |
| 329011 |     Backpack (lvl 1) item texture     |          `minecraft:stick`          |
| 329012 |     Backpack (lvl 2) item texture     |        `minecraft:gunpowder`        |
| 329013 |     Backpack (lvl 3) item texture     |      `minecraft:blaze_powder`       |
| 329014 |     Backpack (lvl 4) item texture     |         `minecraft:jigsaw`          |
| 329015 |     Backpack (lvl 5) item texture     |     `minecraft:structure_block`     |
| 329016 |     Backpack (lvl 6) item texture     | `minecraft:repeating_command_block` |
| 329017 |      Ender Backpack item texture      | `minecraft:repeating_command_block` |
| 329018 |  Basic storage upgrade item texture   |      `minecraft:command_block`      |
| 329019 | Advanced storage upgrade item texture |   `minecraft:chain_command_block`   |


## Heartsteel
|   ID   |         Description         |  Retextured Item  |
| ------ | --------------------------- | ----------------- |
| 329101 | Heartsteel advancement icon | `minecraft:stone` |


## Smite
|   ID   |                   Description                 |             Retextured Item             |
| ------ | --------------------------------------------- | --------------------------------------- |
| 329102 |      Smite item texture/advancement icon      |           `minecraft:raw_iron`          |
| 329103 | Unleashed Smite item texture/advancement icon |         `minecraft:raw_copper`          |
| 329104 |  Primal Smite item texture/advancement icon   |          `minecraft:raw_gold`           |
| 329105 |        Crab Wrangler advancement icon         |            `minecraft:stone`            |


## PlacementGUI
|   ID   |      Description      |    Retextured Item    |
| ------ | --------------------- | --------------------- |
| 329998 | Full black background |  `minecraft:bedrock`  |
| 329999 |    Retry indicator    |  `minecraft:bedrock`  |