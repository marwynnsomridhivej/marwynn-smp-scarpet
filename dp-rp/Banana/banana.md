# Banana v1.0.0
Adds the banana item, an analog to the apple for jungle leaves, along with
recipes to craft golden bananas, enchanted golden bananas, banana bundles, and
the golden and enchanted variants of the banana bundles.

## New Items
| Item | Hunger Restored | Saturation Restored | Effects Upon Consumption | Obtainable via |
| ---- | :-------------: | :-----------------: | :----------------------: | :------------: |
| ![banana image](/dp-rp/Banana/Banana%20-%20RP/assets/marwynn/textures/item/banana/banana.png "Banana") Banana | 2 | 0.8 | Haste I (5s) | Natural decay of jungle leaves |
| ![_image](/dp-rp/Banana/Banana%20-%20RP/assets/marwynn/textures/item/banana/golden_banana.png) Golden Banana | 12 | 8 | Haste II (30s) | [Crafting](#golden-banana) |
| ![_image](/dp-rp/Banana/Banana%20-%20RP/assets/marwynn/textures/item/banana/golden_banana.png) Enchanted Golden Banana| 24 | 24 | Haste IV (300s) | [Crafting](#enchanted-golden-banana) |
| ![_image](/dp-rp/Banana/Banana%20-%20RP/assets/marwynn/textures/item/banana/banana_bundle.png) Banana Bundle | 6 | 2.4 | Haste I (15s) | [Crafting](#banana-bundle) |
| ![_image](/dp-rp/Banana/Banana%20-%20RP/assets/marwynn/textures/item/banana/golden_banana_bundle.png) Golden Banana Bundle | 16 | 12 | Haste II (90s) | [Crafting](#golden-banana-bundle) |
| ![_image](/dp-rp/Banana/Banana%20-%20RP/assets/marwynn/textures/item/banana/golden_banana_bundle.png) Enchanted Golden Banana Bundle | 60 | 60 | Haste IV (900s) | [Crafting](#enchanted-golden-banana-bundle) |

## New Recipes

### Golden Banana
![golden banana recipe image](/img/dp/banana/golden_banana_recipe.png)
- 1 Banana
- 8 Gold Ingots

### Enchanted Golden Banana
![enchanted golden banana recipe image](/img/dp/banana/enchanted_golden_banana_recipe.png)
- 1 Golden Banana
- 8 Gold Blocks

### Banana Bundle
![banana bundle recipe image](/img/dp/banana/banana_bundle_recipe.png)
- 3 Bananas

### Golden Banana Bundle
![golden banana bundle recipe image](/img/dp/banana/golden_banana_bundle_recipe.png)
- 1 Banana Bundle
- 8 Gold Ingots

### Enchanted Golden Banana Bundle
![enchanted golden banana bundle recipe image](/img/dp/banana/enchanted_golden_banana_bundle_recipe.png)
- 1 Golden Banana Bundle
- 8 Gold Blocks

## Files
- [Data Pack](/dp-rp/Banana/Banana%20-%20DP.zip)
- [Resource Pack](/dp-rp/Banana/Banana%20-%20RP.zip)
- [Scarpet Script](/banana.sc)