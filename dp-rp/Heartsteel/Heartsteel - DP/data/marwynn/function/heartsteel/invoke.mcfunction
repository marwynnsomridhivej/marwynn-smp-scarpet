scoreboard players add @s heartsteel.procs 1
execute if score @s heartsteel.procs matches 1 run advancement grant @s only marwynn:first_heartsteel
execute if score @s heartsteel.procs matches 10 run advancement grant @s only marwynn:tenth_heartsteel
execute if score @s heartsteel.procs matches 25 run advancement grant @s only marwynn:twentyfive_heartsteel
execute if score @s heartsteel.procs matches 50 run advancement grant @s only marwynn:fifty_heartsteel
execute if score @s heartsteel.procs matches 100 run advancement grant @s only marwynn:one_hundred_heartsteel

advancement revoke @s only marwynn:heartsteel_proc