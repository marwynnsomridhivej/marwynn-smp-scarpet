# Heartsteel v1.0.0
Adds the Heartsteel enchantment, inspired by the [Heartsteel item](https://wiki.leagueoflegends.com/en-us/Heartsteel)
from League of Legends.

## Mechanics
Similar to how the item works in League of Legends, the Heartsteel enchantment
allows the user to stack additional maximum health on diamond and netherite
chestplates. If the player is wearing a Heartsteel enchanted chestplate, with each
successful attack (i.e. an attack that did damage and was not blocked) on a
hostile mob or other player, there is a chance that the attack will proc Heartsteel,
playing a special proc sound effect and adding one additional max health to the
chestplate.

> ![heartsteel chestplate image](/img/dp/heartsteel/heartsteel_chestplate_equipped.png)<br>
> *A netherite chestplate enchanted with Heartsteel, showing the amount of max 
health accumulated*

Any Heartsteel enchanted chestplate may have a maximum of 100 Heartsteel stacks
(50 bonus hearts). The stacks apply for that particular chestplate, meaning
players may have multiple Heartsteel enchanted chestplates, all with varying
amounts of Heartsteel stacks. Once a chestplate reaches 100 stacks, the player
will no longer be able to gain additional maximum health and Heartsteel will no
longer proc while wearing that particular chestplate.

> ![heartsteel extra hearts image](/img/dp/heartsteel/heartsteel_health_bar.png)<br>
> *A player's HP bar while wearing a Heartsteel enchanted chestplate with 60 
> stacks (30 bonus hearts)*

The bonus health is only applied when the chestplate is worn. Equipping and
unequipping the enchanted chestplate will maintain the player's current percent
health (e.g. equipping a Heartsteel enchanted chestplate with 20 bonus HP while
at 10 current HP will cause your maximum HP to increase to 40 and your current
HP to 20, maintaining the 50% maximum HP ratio). This HP ratio will persist over
disconnections and will be restored upon player login if the player disconnected
while wearing a Heartsteel enchanted chestplate.

The Heartsteel enchantment has 10 levels (I - X). Each level determines the
likelihood of obtaining a Heartsteel proc on-hit. The formula for the is
$`\frac{level^2}{400}`$, where *level* is the Heartsteel enchantment level.

> *Note: the way Heartsteel checks to proc on-hit is on a per-entity-damaged basis.
This means a single attack may proc Heartsteel multiple times if the attack
damaged multiple entities.*

## Methods to Obtain
Heartsteel is considered a treasure enchantment (similar to Mending), and
therefore is obtainable only through villager trading in the form of an
enchanted book. The level of the enchantment that is offered is also randomised,
just like any other enchanted book trade. The Heartsteel enchantment can be
applied to any chestplate.

> ![heartsteel trade image](/img/dp/heartsteel/heartsteel_trade.png)<br>
> *A Heartsteel enchanted book offered by a librarian villager in the trade menu*

## Files
- [Data Pack](/dp-rp/Heartsteel/Heartsteel%20-%20DP.zip)
- [Resource Pack](/dp-rp/Heartsteel/Heartsteel%20-%20RP.zip)
- [Scarpet Script](/heartsteel.sc)