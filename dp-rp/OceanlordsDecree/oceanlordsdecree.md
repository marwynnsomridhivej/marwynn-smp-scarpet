# Oceanlord's Decree v1.0.0
Adds the Oceanlord's Decree boots enchantment, allowing the wearer to gain the
Dolphin's Grace effect for the duration the boots are equipped.

## Mechanics
While the player has equipped a pair of boots enchanted with Oceanlord's Decree,
they will be granted the Dolphin's Grace effect for as long as they are wearing
those boots, even when a dolphin is not nearby.<br>
![oceanlord effect example image](/img/dp/oceanlord/equipped.png)

## Methods to Obtain
Oceanlord's Decree is considered a treasure enchantment (similar to Mending), and
therefore is obtainable only through villager trading in the form of an enchanted
book. The enchantment has a single level, and can be applied to any type of boots.
It is also mutually exclusive with Frost Walker.<br>
![villager trade image](/img/dp/oceanlord/trading.png)