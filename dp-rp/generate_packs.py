import os
import shutil
import sys


if __name__ == "__main__":
    datapack_name = sys.argv[1]
    for filename in os.listdir(f"./{datapack_name}"):
        if filename.endswith(".zip"):
            try:
                os.remove(f"./{datapack_name}/{filename}")
            except PermissionError:
                print(f"Unable to delete {filename} (in use by another process)")
    for _dir in [f"./{datapack_name}/{datapack_name} - {pack_type}" for pack_type in ["DP", "RP"]]:
        if os.path.exists(_dir):
            shutil.make_archive(base_name=_dir, format="zip", root_dir=_dir)
    exit(0)