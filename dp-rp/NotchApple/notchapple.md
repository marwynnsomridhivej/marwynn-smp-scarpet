# Notch Apple v1.0.0
Adds a recipe to allow crafting the enchanted golden apple.

## Recipe
![enchanted golden apple recipe image](/img/dp/notchapple/notchapple_recipe.png)
- 1 Golden Apple
- 8 Gold Blocks

## Files
- [Data Pack](/dp-rp/NotchApple/NotchApple%20-%20DP.zip)