# Custom Music Discs v1.0.0
Adds defined custom music discs that can be crafted from any vanilla music disc
variant and can be played in a jukebox.

## New Songs and Recipes
Contact `msarranges` on Discord to request additional songs. Please provide a
YouTube URL as well as what the recipe for the music disc should be in your
message.

### Clean Bandit - Symphony (feat. Zara Larsson)
![symphony recipe image](/img/dp/custommusicdiscs/symphony_recipe.png)
- 1 Music Disc (any vanilla variant)
- 1 Diamond

### itsDigBar - Three Big Balls
![three big balls recipe image](/img/dp/custommusicdiscs/threebigballs_recipe.png)
- 1 Music Disc (any vanilla variant)
- 1 Netherite Ingot

### KSI - Thick of It (feat. Trippie Redd)
![thick of it image](/img/dp/custommusicdiscs/thickofit_recipe.png)
- 1 Music Disc (any vanilla variant)
- 1 Bedrock


## Files
- [Data Pack](/dp-rp/CustomMusicDiscs/CustomMusicDiscs%20-%20DP.zip)
- [Resource Pack](/dp-rp/CustomMusicDiscs/CustomMusicDiscs%20-%20RP.zip)