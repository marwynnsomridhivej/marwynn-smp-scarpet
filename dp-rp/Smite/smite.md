# Smite v1.0.0
Adds the Smite item, inspired by the League of Legends
[Smite](https://wiki.leagueoflegends.com/en-us/Smite) summoner spell.

## Mechanics
Similar to how the summoner spell works in League of Legends, the player with
Smite equipped on their mainhand is able to attack any entity and summon a lightning bolt to strike
it.

Below are details for each level of smite:
|      Type       | Damage |           Secondary Effect(s)            | Cooldown |
| :-------------: | :----: | :--------------------------------------: | :------: |
|      Smite      |  5 HP  |                   N/A                    |   20s    |
| Unleashed Smite | 12 HP  |                   N/A                    |   15s    |
|  Primal Smite   | 24 HP  | Slowness I (5s)<br>Mining Fatigue I (5s) |   10s    |

## Recipes and Upgrading

### Smite
Base smite is crafted in a crafting table using the following recipe:<br>
![smite recipe image](/img/dp/smite/smite_recipe.png)
- 1 Diamond
- 2 Lightning Rods
- 6 Copper Ingots

### Unleashed Smite
To upgrade base smite into unleashed smite, use a smithing table and do as if
you were upgrading a diamond tool to netherite:<br>
![unleashed smite recipe image](/img/dp/smite/unleashed_smite_recipe.png)
- 1 Netherite Upgrade Smithing Template
- 1 [Smite](#smite)
- 1 Netherite Ingot

### Primal Smite
To upgrade unleashed smite into primal smite, use a smithing table and do as if
you were upgrading a diamond tool to netherite:<br>
![unleashed smite recipe image](/img/dp/smite/primal_smite_recipe.png)
- 1 Netherite Upgrade Smithing Template
- 1 [Unleashed Smite](#unleashed-smite)
- 1 Netherite Ingot 