# Extended Enchantments v1.0.0
Increases the maximum enchantment level of certain vanilla enchantments. The
behavior of the following enchantments respects vanilla behavior as defined
in the [Minecraft Wiki](https://minecraft.wiki/w/Enchanting#Maximum_effective_values_for_enchantments).

## Modifications
| Enchantment | New Maximum Enchantment Level |
| ----------- | :---------------------------: |
| Bane of Arthropods | `10` |
| Blast Protection | `10` |
| Breach | `10` |
| Density | `10` |
| Efficiency | `10` |
| Feather Falling | `7` |
| Fire Aspect | `10` |
| Fire Protection | `10` |
| Fortune | `10` |
| Impaling | `10` |
| Knockback | `10` |
| Looting | `10` |
| Luck of the Sea | `10` |
| Power | `10` |
| Projectile Protection | `10` |
| Protection | `20` |
| Punch | `10` |
| Respiration | `10` |
| Riptide | `10` |
| Sharpness | `10` |
| Smite | `10` |
| Soul Speed\* | `10` |
| Sweeping Edge | `10` |
| Swift Sneak\* | `5` |
| Thorns | `10` |
| Unbreaking | `10` |
| Wind Burst | `10` |

\* *now obtainable via villager trading*

## Files
- [Data Pack](/dp-rp/ExtendedEnchantments/ExtendedEnchantments%20-%20DP.zip)
- [Resource Pack](/dp-rp/ExtendedEnchantments/ExtendedEnchantments%20-%20RP.zip)