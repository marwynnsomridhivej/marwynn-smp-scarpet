import json
from math import ceil
import os
import shutil
import subprocess
import sys

from pydub import AudioSegment


if __name__ == "__main__":
    args = sys.argv[1:]
    if len(args) < 2:
        print("Must supply the youtube URL, filename, and description") # Description should be ARTIST - SONG_NAME
        exit(1)
    url, filename, *description = args
    if os.path.exists(f"{filename}.ogg"):
        os.remove(f"{filename}.ogg")
    subprocess.run(["yt-dlp" , url, "-x", "--audio-format", "vorbis", "--audio-quality", "320K"])

    for file in os.listdir("./"):
        if file.endswith(".ogg"):
            if filename == file.removesuffix(".ogg"):
                filename = filename + "-1"
            subprocess.run(["ffmpeg", "-i", file, "-ac", "1", filename + ".ogg"])
            os.remove(file)

    duration = ceil(AudioSegment.from_file(f"{filename}.ogg").duration_seconds)
    dp_song_json_path = f"./CustomMusicDiscs/CustomMusicDiscs - DP/data/marwynn/jukebox_song/{filename}.json"
    if not os.path.exists(dp_song_json_path):
        with open(dp_song_json_path, "w") as file:
            file.write(json.dumps({
                "sound_event": {
                    "sound_id": f"minecraft:music_disc.{filename}",
                },
                "description": " ".join(description),
                "length_in_seconds": duration,
                "comparator_output": 15,
            }, indent=4))

    dp_recipe_json_path = f"./CustomMusicDiscs/CustomMusicDiscs - DP/data/marwynn/recipe/{filename}.json"
    if not os.path.exists(dp_recipe_json_path):
        with open(dp_recipe_json_path, "w") as file:
            file.write(json.dumps({
                "type": "minecraft:crafting_shapeless",
                "category": "misc",
                "group": "marwynn:music_disc",
                "ingredients": [
                    {
                        "tag": "minecraft:creeper_drop_music_discs"
                    },
                    {
                        "item": "minecraft:bedrock"
                    }
                ],
                "result": {
                    "id": "minecraft:music_disc_5",
                    "count": 1,
                    "components": {
                        "minecraft:jukebox_playable": {
                            "song": f"marwynn:{filename}",
                            "show_in_tooltip": True
                        }
                    }
                }
            }, indent=4))

    shutil.move(f"{filename}.ogg", f"./CustomMusicDiscs/CustomMusicDiscs - RP/assets/minecraft/sounds/records/{filename}.ogg")
    rp_sounds_json_path = f"./CustomMusicDiscs/CustomMusicDiscs - RP/assets/minecraft/sounds.json"
    with open(rp_sounds_json_path, "r") as file:
        data = json.loads(file.read())
    if data.get(f"music_disc.{filename}", None) is None:
        data[f"music_disc.{filename}"] = {
            "sounds": [{
                "name": f"records/{filename}",
            }]
        }
    with open(rp_sounds_json_path, "w") as file:
        file.write(json.dumps(data, indent=4))
    exit(0)