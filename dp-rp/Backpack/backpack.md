# Backpack v1.0.0
Adds upgradeable backpacks and an ender backpack, allowing players to store
varying amounts of items in the space of a single inventory slot, or access their
ender chest contents without needing to have access to a physical ender chest.<br>
![backpack inventory image](/img/dp/backpack/demo.png)<br>
*The contents of a Lvl. 6 maximum upgraded standard backpack*

## Obtaining
The following table details all possible backpack upgrade levels and their
supported capacities:

|   Type   | Upgradeable? | Max Level |     Capacity      |       Obtain by        |
| -------- | :----------: | :-------: | ----------------- | ---------------------- |
| Standard |     Yes      |     6     | 9 * `level` slots | Crafting and upgrading |
|  Ender   |      No      |     1     |     27 slots      |        Crafting        |


The standard Lvl. 1 backpack contains 9 inventory slots, though it can be upgraded
to increase its capacity later on. It can be crafted using the following recipe:<br>
![backpack recipe image](/img/dp/backpack/backpack_lvl1_recipe.png)
- 8 Leather
- 1 Chest

The ender backpack contains 27 slots, with each slot synced to your ender chest.
Any changes made to the ender chest will be reflected in the ender backpack, and
vice versa. The ender backpack can be crafted using the following recipe:<br>
![ender backpack recipe image](/img/dp/backpack/ender_backpack_recipe.png)
- 4 Leather
- 4 Eye of Ender
- 1 Ender Chest

## Storage Upgrade Modules
The standard backpack can be upgraded at a smithing table to increase the amount
of inventory slots it contains. Each upgrade requires either a Basic Storage Upgrade
or an Advanced Storage Upgrade module.

### Basic Storage Upgrade
This smithing template can be used to upgrade your backpacks to Lvl. 2 and Lvl. 3.<br>
![basic storage upgrade recipe](/img/dp/backpack/basic_storage_upgrade_recipe.png)
- 2 Leather
- 4 String
- 3 Chests

### Advanced Storage Upgrade
This smithing template can be used to upgrade your backpacks to levels above 3,
up to a maximum of 6.<br>
![advanced storage upgrade recipe](/img/dp/backpack/advanced_storage_upgrade_recipe.png)
- 4 Chains
- 2 Chests
- 2 Shulker Shells
- 1 Shulker Box (any color)

## Increasing Backpack Capacity
You may upgrade your backpack using the following recipes:

### Lvl. 2
Increases maximum capacity from 9 slots to 18 slots.<br>
![backpack lvl 2 upgrade recipe](/img/dp/backpack/backpack_lvl2_recipe.png)
- 1 Basic Storage Upgrade
- 1 Lvl. 1 Backpack
- 1 Leather

### Lvl. 3
Increases maximum capacity from 18 slots to 27 slots.<br>
![backpack lvl3  upgrade recipe](/img/dp/backpack/backpack_lvl3_recipe.png)
- 1 Basic Storage Upgrade
- 1 Lvl. 2 Backpack
- 1 Iron Ingot

### Lvl. 4
Increases maximum capacity from 27 slots to 36 slots.<br>
![backpack lvl4  upgrade recipe](/img/dp/backpack/backpack_lvl4_recipe.png)
- 1 Advanced Storage Upgrade
- 1 Lvl. 3 Backpack
- 1 Gold Ingot

### Lvl. 5
Increases maximum capacity from 36 slots to 45 slots.<br>
![backpack lvl5  upgrade recipe](/img/dp/backpack/backpack_lvl5_recipe.png)
- 1 Advanced Storage Upgrade
- 1 Lvl. 4 Backpack
- 1 Diamond

### Lvl. 6
Increases maximum capacity from 45 slots to 54 slots.<br>
![backpack lvl6  upgrade recipe](/img/dp/backpack/backpack_lvl6_recipe.png)
- 1 Advanced Storage Upgrade
- 1 Lvl. 5 Backpack
- 1 Netherite Ingot


## Restrictions
The following items are not allowed to be placed inside backpacks:
- Any backpack type (normal or ender)
- Bundles
- Shulker boxes

When the backpack inventory GUI appears, you will be prevented from interacting
with the aforementioned items. This means that while the backpack is opened, the
GUI prevents you from even moving the slot in which any of those items are in,
including the backpack you currently have open.